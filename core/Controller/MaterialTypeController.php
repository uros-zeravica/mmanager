<?php 

namespace Mmanager\Controller;

use Mmanager\Config\Config;

use Mmanager\Model\MaterialModel;

use Mmanager\Model\TempUserModel;
use Mmanager\Model\TempPasswordModel;

    class MaterialTypeController {


    	/*public static function loadMaterialById($db,$id) {

            $material_result = DatabaseController::fetchRow($db, 'SELECT * FROM `material_type` WHERE `id` = ?', [$id], $error);
            return static::toMaterialModel($material_result, $error);

        }

        private static function toMaterialModel($response, $error) {
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            if(empty($response)) {
                throw new \Exception('Material does not exist', 409);
            }

            return MaterialModel::objectFromData($response);

        }*/



       /* public static function nameExists($db, $name) {

            $user_result = DatabaseController::fetchRow($db, "SELECT `id` FROM `material_type` WHERE `name` = ?", [$name], $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return !empty($user_result);

        }*/

        

    public static function getmaterialtype($db) {

        
       
        $material = DatabaseController::fetchAll($db, 
        "SELECT * FROM `material_type` mt INNER JOIN `material_general_type` mtp ON  mt.general_type_id = mtp.id",$error);

        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($material)) {
            throw new \Exception('MaterialTpye does not exist', 409);
        }
       
        return $material; 

    }



}