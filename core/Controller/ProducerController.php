<?php 

namespace Mmanager\Controller;

use Mmanager\Config\Config;

use Mmanager\Model\ProducerModel;

use Mmanager\Model\TempUserModel;
use Mmanager\Model\TempPasswordModel;

    class ProducerController {


    	public static function loadProducerById($db,$id) {

            $producer_result = DatabaseController::fetchRow($db, 'SELECT * FROM `producers` WHERE `id` = ?', [$id], $error);
            return static::toProducerModel($producer_result, $error);

        }

        private static function toProducerModel($response, $error) {
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            if(empty($response)) {
                throw new \Exception('Producer does not exist', 409);
            }

            return ProducerModel::objectFromData($response);

        }



        public static function nameExists($db, $name) {

            $user_result = DatabaseController::fetchRow($db, "SELECT `id` FROM `producers` WHERE `producer_name` = ?", [$name], $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return !empty($user_result);

        }

        public static function emailExists($db, $email) {

        $user_result = DatabaseController::fetchRow($db, "SELECT `id` FROM `producers` WHERE `email` = ?", [$email], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return !empty($user_result);

        }


        public static function createproducer($db, ProducerModel $producer){

            if(ProducerModel::hasRequiredAttributes($producer)) {
                $producer->id = DatabaseController::executeQuery($db, 
                'INSERT INTO `producers` (`producer_name`, `address`, `address1`, `city`, `state`, `country`, `email`, `phone`, `web`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);' , 
                [
                    $producer->name ,
                    $producer->address ,
                    $producer->address1 ,
                    $producer->city ,
                    $producer->state ,
                    $producer->country,
                    $producer->email,
                    $producer->phone,
                    $producer->web
                ],
                $error);
                if(!empty($error)) {
                    throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
                }
                return $producer;

            } else {

                throw new \Exception('Missing required parameters', 400);
            }

        }


        public static function updateproducer($db, ProducerModel $producer, $id) {

        if(ProducerModel::isValid($producer)){     
        $producer->id = $id;
        DatabaseController::executeQuery($db, 'UPDATE `producers` SET `producer_name` = ? , `address` = ? , `address1` = ? , `city` = ? , `state` = ? , `country` = ?,
         `email` = ?,`phone` = ?, `web` = ? WHERE `id` = ?;', 
        [

            $producer->name,
            $producer->address,
            $producer->address1,
            $producer->city,
            $producer->state,
            $producer->country,
            $producer->email,
            $producer->phone,
            $producer->web,
            $producer->id

        ], 
            $error); 

            if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return;
    }
        throw new \Exception('Missing required parameters', 400);

 }

    public static function getOneProducer($db,$id){


        $producer = DatabaseController::fetchRow($db, 
        "SELECT * FROM `producers` p INNER JOIN `countries` c ON  p.country = c.id  WHERE p.id = ?", [$id],$error);

        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($producer)) {
            throw new \Exception('Material does not exist', 409);
        }
       
        return $producer; 
    }


}