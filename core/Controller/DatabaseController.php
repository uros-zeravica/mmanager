<?php

namespace Mmanager\Controller;

use PDO;
use Mmanager\Config\DatabaseConfig;
use Mmanager\Model\Exception;

class DatabaseController {


  public static function getDB() {
    return new PDO( DatabaseConfig::DSN, 
                    DatabaseConfig::USER, 
                    DatabaseConfig::PASSWORD, 
                    [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::ATTR_EMULATE_PREPARES => false] 
    );
  }

  public static function executeStatement($db, $query, $params, &$error) {
    $statement = $db->prepare($query);
    if(!$statement->execute($params)) {
        error_log('SQL Error');
        error_log($statement->queryString);
        error_log(print_r($params,true));
        error_log($statement->errorInfo()[2]);
        $error = $statement->errorInfo();
        return null;
    }
    return $statement;
  }

  public static function executeQuery($db, $query, $params, &$error=null) {
    $isInsertOrUpdate = preg_match('/^(INSERT|UPDATE)/i',$query);
    $statement = static::executeStatement($db, $query, $params, $error);
    if($statement!=null) {
      if($isInsertOrUpdate) {
        return $db->lastInsertId();
      } else {
        return $statement;
      }
    }
    return null;
  }

  public static function fetchRow($db, $query, $params, &$error=null) {
    $statement = static::executeStatement($db, $query, $params, $error);
    if($statement) {
      return $statement->fetch(PDO::FETCH_ASSOC);
    }
    return null;
  }

  public static function fetchAll($db, $query, $params, &$error=null) {
    $statement = static::executeStatement($db, $query, $params, $error);
    if($statement) {
      return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    return null;
  }

  
  
    public static function executeTransaction($db, $queries, $params) {
      $returns = [];
      static::executeQuery($db, 'START TRANSACTION;', array(), $error);
        if(!empty($error)) {
           return array('status'=>400,'description' => (isset($error[2]) ? $error[2] : 'None') );
        } 
        $count = count($queries);
        $i = 0;
        for($i=0;$i++;$i<$count) {
          $tsql = $queries[$i];
          $param = $params[$i];
          $returns[] = static::executeQuery($db, $tsql, $param, $error);
          if(!empty($error)) {
            static::executeQuery($db, 'ROLLBACK;', array(), $error);
            throw new \Exception((isset($error[2]) ? $error[2] : 'None'),500 );
          }
        }
        static::executeQuery($db, 'COMMIT;', array(), $error);
        if(!empty($error)) {
          throw new \Exception((isset($error[2]) ? $error[2] : 'None'),500 );
        }

        return $returns;

    }

}

?>
