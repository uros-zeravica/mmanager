<?php

namespace Mmanager\Controller;

use Mmanager\Config\Config;

class AuthController {

    public static function generateAccessToken() {

        if (function_exists('random_bytes')) {
            $randomData = random_bytes(20);
            if ($randomData !== false && strlen($randomData) === 20) {
                return bin2hex($randomData);
            }
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            $randomData = openssl_random_pseudo_bytes(20);
            if ($randomData !== false && strlen($randomData) === 20) {
                return bin2hex($randomData);
            }
        }
        if (function_exists('mcrypt_create_iv')) {
            $randomData = mcrypt_create_iv(20, MCRYPT_DEV_URANDOM);
            if ($randomData !== false && strlen($randomData) === 20) {
                return bin2hex($randomData);
            }
        }
        if (@file_exists('/dev/urandom')) { // Get 100 bytes of random data
            $randomData = file_get_contents('/dev/urandom', false, null, 0, 20);
            if ($randomData !== false && strlen($randomData) === 20) {
                return bin2hex($randomData);
            }
        }
        // Last resort which you probably should just get rid of:
        $randomData = mt_rand() . mt_rand() . mt_rand() . mt_rand() . microtime(true) . uniqid(mt_rand(), true);

        return substr(hash('sha512', $randomData), 0, 40);
    }

    
    public static function hashPassword($password)
    {
        $options = [ 'cost' => 10 ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public static function verifyPassword($password, $hash)
    {
        return password_verify($password, $hash);
    }
    
    public static function setAccessToken($db, $user_id, $access_token,$user_status){

            // convert expires to datestring
            $tmstp = time() + Config::ACCESS_TOKEN_LIFETIME;
            $expires = date('Y-m-d H:i:s', $tmstp);
            $error = null;

            // if it exists, update it.
            $user = static::getUserByAccessToken($db, $access_token);
            if (!empty($user)) {
                DatabaseController::executeQuery($db,'UPDATE `user_access_token` SET `user_id`=?, `expires`=?  WHERE `access_token`=?', [$user_id, $expires, $access_token], $error);
            } else {
                $access_token = static::generateAccessToken();
                DatabaseController::executeQuery($db, 'INSERT INTO `user_access_token` (`access_token`, `user_id`, `expires`) VALUES (?, ?, ?)',[$access_token, $user_id, $expires], 
                    $error);
            }

            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }

            return [
                'access_token'=> $access_token,
                'expires_in'=> Config::ACCESS_TOKEN_LIFETIME,
                'user_status'=> $user_status
            ];

        }


    public static function unsetAccessToken($db, $access_token){

        DatabaseController::executeQuery($db,'DELETE FROM `user_access_token` WHERE `access_token` = ?', [$access_token],$error);
        return empty($error);

    }


    public static function getUserByAccessToken($db, $access_token)
    {
        $row = DatabaseController::fetchRow($db, 'SELECT user_id, expires FROM `user_access_token` WHERE `access_token` = ?', [$access_token], $error);
        if(!empty($row)) {
            if(strtotime($row['expires']) < time() ) {
                static::unsetAccessToken($db, $access_token);
                return [];
            }
            return $row;
        }
        return []; 
    }

    public static function isAutorised($db) {

        $token = static::getAuthTokenFromHeader();
        if(empty($token)) {
            return false;
        }
        $user = static::getUserByAccessToken($db, $token);
        if(empty($user)) {
            return false;
        }
        return true;
    }

    public static function getAuthUser($db) {

        $token = static::getAuthTokenFromHeader();
        if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
        }
        $user = static::getUserByAccessToken($db, $token);
        if(empty($user) || !isset($user['user_id'])) {
            throw new \Exception('Unauthorized', 401);
        }
        
        $row = DatabaseController::fetchRow($db, 'SELECT * FROM `user` WHERE `id` = ?', [$user['user_id']], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($row)) {
            throw new \Exception('User does not exist', 409);
        }
        return \Mmanager\Model\UserModel::getPublicData($row); 

    }

    

    public static function getAuthUsers($db) {

        $token = static::getAuthTokenFromHeader();
        if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
        }
        $user = static::getUserByAccessToken($db, $token);
        if(empty($user) || !isset($user['user_id'])) {
            throw new \Exception('Unauthorized', 401);
        }
        
        $users = DatabaseController::fetchAll($db, 'SELECT id, username, status, first_name, last_name, created FROM `user`', $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($users)) {
            throw new \Exception('Users does not exist', 409);
        }
       
        return $users; 

    }

    public static function getproducers($db) {

        $token = static::getAuthTokenFromHeader();
        if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
        }
        
        
        $producers = DatabaseController::fetchAll($db, 'SELECT p.id, p.producer_name, p.address, p.address1, p.city, p.state, p.country, p.email, p.phone, p.web,c.name ,
            p.created FROM `producers` p INNER JOIN `countries` c ON p.country = c.id', $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($producers)) {
            throw new \Exception('Producers does not exist', 409);
        }
       
        return $producers; 

    }

    public static function getmaterials($db) {

        $token = static::getAuthTokenFromHeader();
        if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
        }
        $user = static::getUserByAccessToken($db, $token);
        if(empty($user) || !isset($user['user_id'])) {
            throw new \Exception('Unauthorized', 401);
        }

       $sql = "SELECT m.id,m.material_name,m.type_id,m.producer_id,p.producer_name,m.original_width,m.original_height,
       m.original_depth,m.code,m.description_m,m.image_url,m.created,p.producer_name,mt.materialtype_name 
       FROM `materials` m INNER JOIN `producers` p ON  m.producer_id = p.id  INNER JOIN `material_type` mt ON m.type_id = mt.id";
        $materials = DatabaseController::fetchAll($db, $sql, $error);

        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($materials)) {
            throw new \Exception('Materials does not exist', 409);
        }
       
        return $materials; 

    }

        public static function getCountries($db){

        $token = static::getAuthTokenFromHeader();
        if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
        }
        
        $countries = DatabaseController::fetchAll($db, 'SELECT * FROM  `countries`', $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($countries)) {
            throw new \Exception('Country does not exist', 409);
        }
        return $countries; 

    }

    public static function generatePin($digits)
    {
        return str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    }

    public static function generateGUID() {
    
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
            //    .chr(125);// "}"
            return $uuid;
        }
        
    }

    public static function getAuthTokenFromHeader() {

        $access_token = isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : NULL;
        if(!empty($access_token)) {
            if (strpos($access_token, 'Bearer ') !== false) {
                $token = str_replace('Bearer ','', $access_token);
                return $token;
            }
        }
        return NULL;

    }

}

?>