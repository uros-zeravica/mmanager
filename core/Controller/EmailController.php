<?php 

namespace Mmanager\Controller;

require_once __DIR__ . '/../Config/EmailConfig.php';

use Mmanager\Config\Config;
use Mmanager\Config\EmailConfig;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Mmanager\Controller\LanguageController;

class EmailController {

    public $options;
    public $email_from;
    public $name_from;
    
    public static function getMail () {
  
        $mail = new PHPMailer(true);   
        $mail->SMTPDebug = 0; // Enable verbose debug output
        $mail->isSMTP();
        $mail->Host = EmailConfig::EMAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = EmailConfig::EMAIL_USERNAME;
        $mail->Password = EmailConfig::EMAIL_PASSWORD;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = EmailConfig::EMAIL_PORT;
        $mail->setFrom(EmailConfig::EMAIL_FROM_ADDRESS, EmailConfig::EMAIL_FROM_NAME);
        $mail->addReplyTo(EmailConfig::EMAIL_REPLY_TO, EmailConfig::EMAIL_FROM_NAME);
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        return $mail;
    }
    
    public static function welcomeMail($user_email, $activation_code) {
        
        $messages = LanguageController::getText();
        $activation_link = Config::ROOT_URL . "/profile/activation/$activation_code?lang=" . LanguageController::getLang();

        $content = $messages['welcome_part1']  .
                    '<br><br>' . $messages['welcome_part2']. 
                    '<br><br><a href="' . $activation_link . '">Confirm</a>';
        
        $mail = static::getMail();

        $mail->addAddress($user_email);
        $mail->Subject = $messages['welcome_subject'];
        $mail->Body    = $content;
        $mail->AltBody =  $messages['welcome_part2'] . ' ' . $activation_link . '';

        $mail->send();
        
        return true;
    }
    
    public static function resetPasswordMail($user_email, $token, $password) {

        $messages = LanguageController::getText();
        $activation_link = Config::ROOT_URL . "/pages/verify-password.php?token=$token&pin=$password&lang=" . LanguageController::getLang();

        $content = $messages['reset_part1']  .
                '<br>Your Activation Link:<br>' . $messages['reset_part2'] . '<br><br><a href="' . $activation_link .'">' . $activation_link .'</a><br><hr>'.
                $messages['reset_part3'];

        $mail = static::getMail();

        $mail->addAddress($user_email);
        $mail->Subject = $messages['reset_subject'];
        $mail->Body    = $content;
        $mail->AltBody =  $messages['reset_part1'] . ' ' . $activation_link . '. ' . $messages['reset_part3'] . $password;

        if (!$mail->send()) {
            throw new \Exception('Mailer Error: ' . $mail->ErrorInfo, 400);
        }
        
        return true;
    }
    
    public static function invitationMail($user_email, $content, $subject) {
        
        $mail = static::getMail();

        $mail->Subject = $subject;
        $mail->Body    = $content;
        $mail->AltBody =  '';

        $mail->send();
        
        return true;
    }
         
}