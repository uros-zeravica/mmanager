<?php

namespace Mmanager\Controller;

use Mmanager\Config\Config;

class LanguageController {
    
    const DEFAULT_LANG = 'en';
    const ACCEPT_LANGS = ['en', 'de'];

    public static function getLang() {
        $language = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        if(empty($language) || strlen($language)<2) {
            $language = static::DEFAULT_LANG;
        }
        $lang = strtolower(substr($language, 0, 2));
        if(!in_array($lang, static::ACCEPT_LANGS)) {
            $lang = static::DEFAULT_LANG;
        }
        return $lang;
    }

    public static function getText($language='') {
        if(empty($language)) {
            $language = static::getLang();
        }
        $lang = strtolower(substr($language, 0, 2));
        if(!in_array($lang, static::ACCEPT_LANGS)) {
            $lang = static::DEFAULT_LANG;
        }
        $data = file_get_contents(Config::ROOT_FOLDER . "data/language/$lang.json");
        $messages = json_decode($data, true);

        return $messages;
    }
    
}