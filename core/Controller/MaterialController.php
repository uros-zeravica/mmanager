<?php 

namespace Mmanager\Controller;

use Mmanager\Config\Config;

use Mmanager\Model\MaterialModel;

use Mmanager\Model\TempUserModel;
use Mmanager\Model\TempPasswordModel;

    class MaterialController {


    	public static function loadMaterialById($db,$id) {

            $material_result = DatabaseController::fetchRow($db, 'SELECT * FROM `materials` WHERE `id` = ?', [$id], $error);
            return static::toMaterialModel($material_result, $error);

        }

        private static function toMaterialModel($response, $error) {
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            if(empty($response)) {
                throw new \Exception('Material does not exist', 409);
            }

            return MaterialModel::objectFromData($response);

        }



        public static function nameExists($db, $name) {

            $user_result = DatabaseController::fetchRow($db, "SELECT `id` FROM `materials` WHERE `material_name` = ?", [$name], $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return !empty($user_result);

        }

        public static function creatematerial($db, MaterialModel $material){

            if(MaterialModel::hasRequiredAttributes($material)) {
                $material->id = DatabaseController::executeQuery($db, 
                'INSERT INTO `materials` (`material_name`, `type_id`, `producer_id`, `original_width`, `original_height`, `original_depth`, `code`,
                 `description_m`, `image_url`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);', 
                [
                    $material->material_name ,
                    $material->type_id ,
                    $material->producer_id ,
                    $material->original_width ,
                    $material->original_height ,
                    $material->original_depth,
                    $material->code,
                    $material->description_m,
                    $material->image_url
                ],
                $error);
                if(!empty($error)) {
                    throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
                }
                return $material;

            } else {

                throw new \Exception('Missing required parameters', 400);
            }

        }


        public static function updatematerial($db, MaterialModel $material, $id) {

        if(MaterialModel::isValid($material)){     
        $material->id = $id;
        DatabaseController::executeQuery($db, 'UPDATE `materials` SET `material_name` = ? , `type_id` = ? , `producer_id` = ? , `original_width` = ? , `original_height` = ? , `original_depth` = ?,`code` = ?,`description_m` = ?, `image_url` = ? WHERE `id` = ?;', 
        [

            $material->material_name,
            $material->type_id,
            $material->producer_id,
            $material->original_width,
            $material->original_height,
            $material->original_depth,
            $material->code,
            $material->description_m,
            $material->image_url,
            $material->id

        ], 
            $error); 

            if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return;
    }
        throw new \Exception('Missing required parameters', 400);

 }

    public static function getmaterial($db,$id) {

        
       
        $material = DatabaseController::fetchRow($db, 
        "SELECT m.id,m.material_name,m.type_id,m.producer_id,p.producer_name,m.original_width,m.original_height,
        m.original_depth,m.code,m.description_m,m.image_url,m.created,p.producer_name,mt.materialtype_name
        FROM `materials` m INNER JOIN `producers` p ON  m.producer_id = p.id  INNER JOIN `material_type` mt ON m.type_id = mt.id WHERE m.id = ?", [$id],$error);

        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($material)) {
            throw new \Exception('Material does not exist', 409);
        }
       
        return $material; 

    }



}