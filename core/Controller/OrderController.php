<?php
namespace Mmanager\Controller;
        
       
        use Mmanager\Model\OrderModel;

        

        class OrderController {


        public static function createOrder($db, OrderModel $order) {
        
     
            
            $order->id = DatabaseController::executeQuery($db, 
            'INSERT INTO `order` (`user_id`, `date_created`, `date_in_progress`, `date_finished`, `date_delivery`,`status_id`,`payment`,`price`,`delivery_type_id`
            ,`supplied_by_users_id`,`delivery_id`,`requested_date`,`photo_attach_image`,`price_delivery`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', 
            [
                $order->user_id ,
                $order->date_created,
                $order->date_in_progress,
                $order->date_finished,
                $order->date_delivery,
                $order->status_id,
                $order->payment,
                $order->price,
                $order->delivery_type_id,
                $order->supplied_by_users_id,
                $order->delivery_id,
                $order->requested_date,
                $order->photo_attach_image,
                $order->price_delivery
                
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
        }


        public static function createOrderItems($db,$data){


            $orderItemsId = $db->lastInsertId();
        
            $materials_custom_id = 0;
            $items = $data->orderItems;

           foreach($items as $item){

            $quantity = $item->quantity;
            $photo_attach_image = $item->photo_attach_image;
            for($i=0;$i<count($item->items);$i++){

            $material_id  = $item->items[$i]->material_id;        
                
            DatabaseController::executeQuery($db, 
            'INSERT INTO `order_items` (`$orderItemsId`, `material_id`, `quantity`, `materials_custom_id`, `photo_attach_image`) VALUES (?, ?, ?, ?, ?);', 
            [
                $orderItemsId ,
                $material_id,
                $quantity,
                $materials_custom_id,
                $photo_attach_image
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
                    }
                }
            }
        }

        public static function createOrderItemsElements($db,$data){ 

            $order_items_id = $db->lastInsertId();

            $data = json_decode($data);
            $items = $data->orderItems;

           foreach($items as $item){
            for($i=0;$i<count($item->items);$i++){

            $quantity  = $item->items[$i]->quantity;
            $width      = $item->items[$i]->width;
            $height     = $item->items[$i]->height;
            $direction  = $item->items[$i]->direction;

        
                
                
            DatabaseController::executeQuery($db, 
            'INSERT INTO `order_items_elements` (`$order_items_id`,`quantity`, `width`, `height`, `direction`) VALUES (?, ?, ?, ?, ?);', 
            [
                $order_items_id ,
                $quantity,
                $width,
                $height,
                $direction
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
                    }
                }
            }
        }

    }

    ?>