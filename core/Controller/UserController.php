<?php 

namespace Mmanager\Controller;

use Mmanager\Config\Config;

use Mmanager\Model\UserModel;
use Mmanager\Model\UserStatus;
use Mmanager\Model\UserRole;
use Mmanager\Model\TempUserModel;
use Mmanager\Model\TempPasswordModel;

class UserController {

    public static function loadUserById($db,$id) {

        $user_result = DatabaseController::fetchRow($db, 'SELECT * FROM `user` WHERE `id` = ?', [$id], $error);
        return static::toUserModel($user_result, $error);

    }

    public static function loadUserByUsername($db,$username) {

        $user_result = DatabaseController::fetchRow($db, 'SELECT * FROM `user` WHERE `username` = ?', [$username], $error);
        return static::toUserModel($user_result,$error);

    }

    public static function loadUserByFbId($db,$fb_id) {

        $user_result = DatabaseController::fetchRow($db, 'SELECT * FROM `user` WHERE `fb_id`= ?', [$fb_id], $error);
        return static::toUserModel($user_result,$error);

    }

    public static function loadUserByCode($db, $activation_code) {
        $user_result = DatabaseController::fetchRow($db, 'SELECT * FROM `temp_user` WHERE `activation_code`= ?', [$activation_code], $error);
        return static::toUserModel($user_result,$error);
    }


    public static function findTempPasswordByToken($db, $token) {
        $result = DatabaseController::fetchRow($db, 'SELECT * FROM `temp_password` WHERE `token`= ?', [$token], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($result)) {
            throw new \Exception('Token is not valid', 409);
        }
        return TempPasswordModel::objectFromData($result);

    }

    public static function usernameExists($db, $username) {

        $user_result = DatabaseController::fetchRow($db, "SELECT `id` FROM `user` WHERE `username` = ?", [$username], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return !empty($user_result);

    }

    private static function toUserModel($response, $error) {
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($response)) {
            throw new \Exception('User does not exist', 409);
        }

        return UserModel::objectFromData($response);

    }
    

    public static function createUser($db, UserModel $user, $hasPassword = true) {
        
        if(UserModel::hasRequiredAttributes($user)) {
            
            $user->id = DatabaseController::executeQuery($db, 
            'INSERT INTO `user` (`username`, `password`, `first_name`, `last_name`, `status`) VALUES (?, ?, ?, ?, ?);', 
            [
                $user->username ,
                $hasPassword ? AuthController::hashPassword($user->password) : $user->password,
                $user->first_name,
                $user->last_name,
                UserStatus::ACTIVE
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return $user;
        }
        throw new \Exception('Missing required parameters', 400);
    }

    public static function updateUser($db, $user_data) {

        if(UserModel::isValid($user_data)) {
            $set = [];
            $values = [];
            $i = 0;
            foreach($user_data as $key => $value){
                if($key!='id' && $key!='username' && $key!='password' && $value!=null) {
                    $set[$i] = '`' . $key . '` = ?';
                    $values[$i] = $value;
                    $i++;
                }
            }
            $params = array_merge($values, [$user_data['id']]);
            DatabaseController::executeQuery($db, 'UPDATE `user` SET ' . implode(',', $set) . ' WHERE id = ?;', $params, $error); 
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return;
        }
        throw new \Exception('User is not valid', 400);
    }

    public static function updateUserPassword($db, $user_id, $password) {

        DatabaseController::executeQuery($db, 'UPDATE `user` SET `password` = ? WHERE `id` = ?;', [AuthController::hashPassword($password),$user_id], $error); 
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return;
        
    }

    public static function deleteUser($db, $user_id) {
        
        DatabaseController::executeQuery($db, 'UPDATE `user` SET `status` = ?, WHERE id = ?;', 
        [
            UserStatus::DELETED,
            $user_id
        ],
        $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return;
        
    }

    public static function createTempUser($db, TempUserModel $user){

        if(UserModel::hasRequiredAttributes($user)) {
            $user->id = DatabaseController::executeQuery($db, 
            'INSERT INTO `temp_user` (`username`, `password`, `first_name`, `last_name`, `activation_code`) VALUES (?, ?, ?, ?, ?);' , 
            [
                $user->username ,
                AuthController::hashPassword($user->password),
                $user->first_name,
                $user->last_name,
                $user->activation_code
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return $user;

        } else {

            throw new \Exception('Missing required parameters', 400);
        }


    }

    public static function removeTempUser($db, $activation_code){
    
        DatabaseController::executeQuery($db, 'DELETE FROM `temp_user` WHERE `activation_code` = ?;', 
        [
            $activation_code
        ],
        $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return;

    }

    public static function createTempPassword($db, TempPasswordModel $password){

        if(TempPasswordModel::hasRequiredAttributes($password)) {
            DatabaseController::executeQuery($db, 
            'INSERT INTO `temp_password` (`user_id`, `password`, `token`) VALUES (?, ?, ?);', 
            [
                $password->user_id ,
                AuthController::hashPassword($password->password),
                $password->token,
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return;
        }
        throw new \Exception('Missing required parameters', 400);

    }

    public static function removeTempPassword($db, $token){
    
        DatabaseController::executeQuery($db, 'DELETE FROM `temp_password` WHERE `token` = ?;', [$token], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        return;

    }


    public static function createUserToken($db, $user_id){

        $token = AuthController::generateAccessToken();
        $tmstp = time() + Config::ACCESS_TOKEN_LIFETIME;
        $expires = date('Y-m-d H:i:s', $tmstp);

        DatabaseController::executeQuery($db, 
            'INSERT INTO `token` (`id`, `user_id`, `expires`) VALUES (?, ?, ?);', 
            [
                $token,
                $user_id,
                $expires
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return [
                'token' => $token,
                'expires' => $expires
            ];

    }

    public static function removeExpiredTokens($db){

        $expires = date('Y-m-d H:i:s', time());
        DatabaseController::executeQuery($db, 
            'DELETE FROM `token` WHERE `expires` < ?;', 
            [
                $expires
            ],
            $error);
            if(!empty($error)) {
                throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
            }
            return;

    }

    public static function findUserToken($db, $token) {
        $result = DatabaseController::fetchRow($db, 
        'SELECT `user_id` FROM `token` WHERE `id`= ?', 
            [
                $token
            ], 
            $error
        );
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500);
        }
        if(empty($result)) {
            throw new \Exception('Token is invalid', 409);
        }
        return $result;
    }
    
    
}


?>