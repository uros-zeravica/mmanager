<?php

namespace Mmanager\Config;

class DatabaseConfig {

  const HOST = 'localhost';
  const PORT = '3306';
  const DATABASE = 'mmanager';
  const USER = 'root';
  const PASSWORD = 'root';

  const DSN = 'mysql:host=' . DatabaseConfig::HOST . ':' . DatabaseConfig::PORT . ';dbname=' . DatabaseConfig::DATABASE;


}

?>
