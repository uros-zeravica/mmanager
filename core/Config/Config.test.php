<?php
namespace Mmanager\Config;

class Config {

  const ROOT_URL = 'https://betterears-dev.appsolute.de';
  const ROOT_FOLDER = '/var/www/betterears-cloud.appsolute.de/';

  const ACCESS_TOKEN_LIFETIME = 2592000;
  
  const FB_APP_ID = '150560771787719';
  const FB_APP_SECRET = 'e9badd2552067f64eea3d2d4949e53fa';
  const FB_VERSION = 'v2.9';

  const WELCOME_PAGE = self::ROOT_URL . '/welcome.php';

}

?>