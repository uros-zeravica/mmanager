<?php

    namespace Mmanager\Model;


    class OrderModel {

    public $id;
    public $user_id;
    public $date_created;
    public $date_in_progress;
    public $date_finished;
    public $date_delivery;
    public $status_id;
    public $payment;
    public $price;
    public $delivery_type_id;
    public $supplied_by_users_id;
    public $delivery_id;
    public $requested_date;
    public $photo_attach_image;
    public $price_delivery;



    public static function objectFromData($order_data){


        $order = new OrderModel();

        $order->id = $order_data['id'];
        $order->user_id = $order_data['user_id'];
        $order->date_created = $order_data['date_created'];
        $order->date_in_progress =  $order_data['date_in_progress'];
        $order->date_finished =  $order_data['date_finished'];
        $order->date_delivery =  $order_data['date_delivery'];
        $order->status_id =  isset($order_data['status_id']) ? $order_data['status_id'] : 1;
        $order->payment =  isset($order_data['payment']) ? $order_data['payment'] : 0;
        $order->price = isset($order_data['price']) ? $order_data['price'] : 0;
        $order->price_delivery =  isset($order_data['price_delivery']) ? $order_data['price_delivery'] : 0;
        $order->delivery_id =  isset($order_data['delivery_id']) ? $order_data['delivery_id'] : 1;
        $order->delivery_type_id = $order_data['delivery_type_id'];
        $order->supplied_by_users_id =  isset($order_data['supplied_by_users_id']) ? $order_data['supplied_by_users_id'] : 0;   
        $order->requested_date =  $order_data['requested_date'];
        $order->photo_attach_image =  $order_data['photo_attach_image'];

        
        return $order;

    }

    public static function dataFromObject($order) {
        return [
            'id' => $order->id,
            'user_id' => $order->user_id,
            'date_created' => $order->date_created,
            'date_in_progress' => $order->date_in_progress,
            'date_finished' => $order->date_finished,
            'date_delivery' => $order->date_delivery,
            'status_id' => $order->status_id,
            'payment' => $order->payment,
            'price' => $order->price,
            'price_delivery' => $order->price_delivery,
            'delivery_id' => $order->delivery_id,
            'delivery_type_id' => $order->delivery_type_id,
            'supplied_by_users_id' => $order->supplied_by_users_id,
            'requested_date' => $order->requested_date,
            'photo_attach_image' => $order->photo_attach_image

        ];
    }

    public static function isValid($order) {
        if(is_array($order)) {
            return ( isset($order['date_in_progress']) && isset($order['user_id']));
        }
        return (isset($order->user_id) && isset($order->date_in_progress));
    }
    
    public static function hasRequiredAttributes($order) {
        if(is_array($order)) {
            return ( isset($order['delivery_type_id']) && isset($order['user_id']));
            
        }
        return ( isset($order->delivery_type_id) && isset($order->user_id));
    }


    public static function getPublicData($order_data) {
        $out = [];
        if(isset($order_data['id'])) {
            $out['id'] = $order_data['id'];
        }

        if(isset($order_data['user_id'])) {
            $out['user_id'] = $order_data['user_id'];
        }

        if(isset($order_data['date_created'])) {
            $out['date_created'] = $order_data['date_created'];
        }
         
        if(isset($order_data['date_in_progress'])) {
            $out['date_in_progress'] = $order_data['date_in_progress'];
        }

        if(isset($order_data['date_finished'])) {
            $out['date_finished'] = $order_data['date_finished'];
        }

        if(isset($order_data['date_delivery'])) {
            $out['date_delivery'] = $order_data['date_delivery'];
        }

        if(isset($order_data['status_id'])) {
            $out['status_id'] = $order_data['status_id'];
        }

        if(isset($order_data['payment'])) {
            $out['payment'] = $order_data['payment'];
        }

        if(isset($order_data['price'])) {
            $out['price'] = $order_data['price'];
        }

        if(isset($order_data['price_delivery'])) {
            $out['price_delivery'] = $order_data['price_delivery'];
        }
        if(isset($order_data['delivery_id'])) {
            $out['delivery_id'] = $order_data['delivery_id'];
        }
        if(isset($order_data['delivery_type_id'])) {
            $out['delivery_type_id'] = $order_data['delivery_type_id'];
        }
        if(isset($order_data['supplied_by_users_id'])) {
            $out['supplied_by_users_id'] = $order_data['supplied_by_users_id'];
        }
        if(isset($order_data['requested_date'])) {
            $out['requested_date'] = $order_data['requested_date'];
        }
        if(isset($order_data['photo_attach_image'])) {
            $out['photo_attach_image'] = $order_data['photo_attach_image'];
        }

        return $out;
    }
    


    public static function setFromPublicData($order_data) {

        $order = new OrderModel();
        

        $order->user_id =  $order_data['user_id'];
        $order->date_created =  $order_data['date_created'];
        $order->date_in_progress =  $order_data['date_in_progress'];
        $order->date_finished =  $order_data['date_finished'];
        $order->date_delivery =  $order_data['date_delivery'];
        $order->status_id =  isset($order_data['status_id']) ? $order_data['status_id'] : 1;
        $order->payment =  isset($order_data['payment']) ? $order_data['payment'] : 0;
        $order->price =   isset($order_data['price']) ? $order_data['price'] : 0;
        $order->price_delivery =  isset($order_data['price_delivery']) ? $order_data['price_delivery'] : 0;
        $order->delivery_type_id =  $order_data['delivery_type_id'];
        $order->delivery_id =  isset($order_data['delivery_id']) ? $order_data['delivery_id'] : 1;
        $order->supplied_by_users_id =  isset($order_data['supplied_by_users_id']) ? $order_data['supplied_by_users_id'] : 0;
        $order->requested_date =  $order_data['requested_date'];
        $order->photo_attach_image =  $order_data['photo_attach_image'];

        return $order;
    }

}