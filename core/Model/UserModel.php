<?php

namespace Mmanager\Model;

abstract class UserStatus  {
    const ACTIVE = 0;
    const INACTIVE = 1;
    const DELETED = 2;
}

class UserModel {
    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $status;
    public $modified;
    public $created;


    public static function objectFromData($user_data) {
        $user = new UserModel();

        $user->id = $user_data['id'];
        $user->username = $user_data['username'];
        $user->password = $user_data['password'];
        $user->first_name = $user_data['first_name'];
        $user->last_name = $user_data['last_name'];
        $user->role = isset($user_data['role']) ? $user_data['role'] : null;
        $user->modified = isset($user_data['modified']) ? $user_data['modified'] : null;
        $user->created = isset($user_data['created']) ? $user_data['created'] : null;
        
        return $user;

    }

    public static function dataFromObject($user) {
        return [
            'id' => $user->id,
            'username' => $user->username,
            'password' => $user->password,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'role' => $user->role
        ];
    }

    public static function isValid($user) {
        if(is_array($user)) {
            return ( isset($user['username']) && isset($user['id']));
        }
        return ( isset($user->id) && isset($user->username));
    }
    
    public static function hasRequiredAttributes($user) {
        if(is_array($user)) {
            return ( isset($user['username']) && isset($user['password']));
            
        }
        return ( isset($user->username) && isset($user->password));
    }

    public static function isValidUsername($username) {
        return filter_var($username, FILTER_VALIDATE_EMAIL);
    }


    public static function getPublicData($user_data) {
        $out = [];
        if(isset($user_data['id'])) {
            $out['id'] = $user_data['id'];
        }

        if(isset($user_data['username'])) {
            $out['username'] = $user_data['username'];
        }

        if(isset($user_data['first_name'])) {
            $out['first_name'] = $user_data['first_name'];
        }
         
        if(isset($user_data['last_name'])) {
            $out['last_name'] = $user_data['last_name'];
        }

        return $out;
    }
    


    public static function setFromPublicData($user_data) {
        $user = new UserModel();
        $user->username =  $user_data['username'];
        $user->password =  $user_data['password'];
        $user->first_name =  $user_data['first_name'];
        $user->last_name =  $user_data['last_name'];
        return $user;
    }

}