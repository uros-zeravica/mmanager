<?php

namespace Mmanager\Model;


class MaterialModel {
    public $id;
    public $material_name;
    public $type_id;
    public $producer_id;
    public $original_width;
    public $original_height;
    public $original_depth;
    public $code;
    public $description_m;
    public $image_url;
    public $modified;
    public $created;


    public static function objectFromData($material_data) {


        $material = new MaterialModel();

        $material->id = $material_data['id'];
        $material->material_name = $material_data['material_name'];
        $material->type_id = $material_data['type_id'];
        $material->producer_id =  $material_data['producer_id'];
        $material->original_width =  $material_data['original_width'];
        $material->original_height =  $material_data['original_height'];
        $material->original_depth =  $material_data['original_depth'];
        $material->code =  $material_data['code'];
        $material->description_m = $material_data['description_m'];
        $material->image_url =  $material_data['image_url'];
        $material->modified = isset($material_data['modified']) ? $material_data['modified'] : null;
        $material->created = isset($material_data['created']) ? $material_data['created'] : null;
        
        return $material;

    }

    public static function dataFromObject($material) {
        return [
            'id' => $material->id,
            'material_name' => $material->material_name,
            'type_id' => $material->type_id,
            'producer_id' => $material->producer_id,
            'original_width' => $material->original_width,
            'original_height' => $material->original_height,
            'original_depth' => $material->original_depth,
            'code' => $material->code,
            'description_m' => $material->description_m,
            'image_url' => $material->image_url
        ];
    }

    public static function isValid($material) {
        if(is_array($material)) {
            return ( isset($material['producer_id']) && isset($material['material_name']));
        }
        return (isset($material->material_name) && isset($material->producer_id));
    }
    
    public static function hasRequiredAttributes($material) {
        if(is_array($material)) {
            return ( isset($material['producer_id']) && isset($material['material_name']));
            
        }
        return ( isset($material->producer_id) && isset($material->material_name));
    }


    public static function getPublicData($material_data) {
        $out = [];
        if(isset($material_data['id'])) {
            $out['id'] = $material_data['id'];
        }

        if(isset($material_data['material_name'])) {
            $out['material_name'] = $material_data['material_name'];
        }

        if(isset($material_data['type_id'])) {
            $out['type_id'] = $material_data['type_id'];
        }
         
        if(isset($material_data['producer_id'])) {
            $out['producer_id'] = $material_data['producer_id'];
        }

        if(isset($material_data['original_width'])) {
            $out['original_width'] = $material_data['original_width'];
        }

        if(isset($material_data['original_height'])) {
            $out['original_height'] = $material_data['original_height'];
        }

        if(isset($material_data['original_depth'])) {
            $out['original_depth'] = $material_data['original_depth'];
        }

        if(isset($material_data['code'])) {
            $out['code'] = $material_data['code'];
        }

        if(isset($material_data['description_m'])) {
            $out['description_m'] = $material_data['description_m'];
        }

        if(isset($material_data['image_url'])) {
            $out['image_url'] = $material_data['image_url'];
        }

        return $out;
    }
    


    public static function setFromPublicData($material_data) {

        $material = new MaterialModel();
        

        $material->material_name =  $material_data['material_name'];
        $material->type_id =  $material_data['type_id'];
        $material->producer_id =  $material_data['producer_id'];
        $material->original_width =  $material_data['original_width'];
        $material->original_height =  $material_data['original_height'];
        $material->original_depth =  $material_data['original_depth'];
        $material->code =  $material_data['code'];
        $material->description_m =  $material_data['description_m'];
        $material->image_url =  $material_data['image_url'];

        return $material;
    }

}