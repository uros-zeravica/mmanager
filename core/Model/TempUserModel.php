<?php

namespace Mmanager\Model;

class TempUserModel {
    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $modified;
    public $created;

    
    public static function setFromPublicData($user_data) {
        $user = new TempUserModel();
        $user->username =  $user_data['username'];
        $user->password =  $user_data['password'];
        $user->first_name =  isset($user_data['first_name']) ? $user_data['first_name'] : '';
        $user->last_name =  isset($user_data['last_name']) ? $user_data['last_name'] : '';
        return $user;
    }

}