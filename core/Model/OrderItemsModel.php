<?php

namespace Mmanager\Model;



class OrderItemsModel {

    public $id;
    public $order_id;
    public $material_id;
    public $quantity;
    public $materials_custom_id;
    public $photo_attach_image;



    public static function getItemsData($orderItems_data) {

        $dataItem = $orderItems_data['orderItems'][0];
        $orderitems = [];
        $orderitems['quantity'] = $dataItem['quantity'];
        $orderitems['materials_cutom_id'] = $dataItem['materials_cutom_id'];
        $orderitems['photo_attach_image'] = $dataItem['photo_attach_image'];
        foreach ($dataItem['items'] as $key => $value) {
            if ($key = 'material_id') {

                $orderitems['materials'] = $value['material_id'];
                
            }
        }
  
            return $orderitems['materials'];

    }

    public static function objectFromData($orderItems_data) {
        $orderItems = new OrderItemsModel();

        $orderItems->id = $orderItems_data['id'];
        $orderItems->order_id = $orderItems_data['order_id'];
        $orderItems->material_id = $orderItems_data['material_id'];
        $orderItems->quantity = $orderItems_data['quantity'];
        $orderItems->materials_custom_id = $orderItems_data['materials_custom_id'];
        $orderItems->photo_attach_image = isset($orderItems_data['photo_attach_image']) ? $orderItems_data['photo_attach_image'] : null;      
        
        return $orderItems;

    }

    public static function dataFromObject($orderItems) {
        return [
            'id' => $orderItems->id,
            'order_id' => $orderItems->order_id,
            'material_id' => $orderItems->material_id,
            'quantity' => $orderItems->quantity,
            'materials_custom_id' => $orderItems->materials_custom_id,
            'photo_attach_image' => $orderItems->photo_attach_image
        ];
    }

    public static function isValid($orderItems) {
        if(is_array($orderItems)) {
            return ( isset($orderItems['order_id']) && isset($orderItems['material_id']));
        }
        return ( isset($orderItems->material_id) && isset($orderItems->order_id));
    }
    
    public static function hasRequiredAttributes($orderItems) {
        if(is_array($orderItems)) {
            return ( isset($orderItems['order_id']) && isset($orderItems['material_id']));
            
        }
        return ( isset($orderItems->order_id) && isset($orderItems->material_id));
    }


    public static function getPublicData($orderItems_data) {
        $out = [];
        if(isset($orderItems_data['id'])) {
            $out['id'] = $orderItems_data['id'];
        }

        if(isset($orderItems_data['order_id'])) {
            $out['order_id'] = $orderItems_data['order_id'];
        }

        if(isset($orderItems_data['quantity'])) {
            $out['quantity'] = $orderItems_data['quantity'];
        }
         
        if(isset($orderItems_data['materials_custom_id'])) {
            $out['materials_custom_id'] = $orderItems_data['materials_custom_id'];
        }

        if(isset($orderItems_data['photo_attach_image'])) {
            $out['photo_attach_image'] = $orderItems_data['photo_attach_image'];
        }

        return $out;
    }
    


    public static function setFromPublicData($orderItems_data) {
        $orderItems = new OrderItemsModel();
        $orderItems->order_id =  $orderItems_data['order_id'];
        $orderItems->material_id =  $orderItems_data['material_id'];
        $orderItems->quantity =  $orderItems_data['quantity'];
        $orderItems->materials_custom_id =  $orderItems_data['materials_custom_id'];
        $orderItems->photo_attach_image =  $orderItems_data['photo_attach_image'];
        return $orderItems;
    }

}