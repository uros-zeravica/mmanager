<?php

namespace Mmanager\Model;

class TempPasswordModel {
    public $id;
    public $user_id;
    public $password;
    public $token;
    public $created;

    public static function hasRequiredAttributes($user) {
        if(is_array($user)) {
            return ( isset($user['password']) && isset($user['token']) && isset($user['user_id']));
            
        }
        return ( isset($user->password) && isset($user->token) & isset($user->user_id));
    }

    public static function objectFromData($user_data) {
        $user = new TempPasswordModel();

        $user->id = $user_data['id'];
        $user->user_id = $user_data['user_id'];
        $user->password = $user_data['password'];
        $user->token = $user_data['token'];
        $user->created = $user_data['created'];
        
        return $user;

    }

}