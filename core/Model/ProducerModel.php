<?php

namespace Mmanager\Model;


class ProducerModel {
    public $id;
    public $name;
    public $address;
    public $address1;
    public $city;
    public $state;
    public $country;
    public $email;
    public $phone;
    public $web;
    public $modified;
    public $created;


    public static function objectFromData($producer_data) {


        $producer = new ProducerModel();

        $producer->id = $producer_data['id'];
        $producer->name = $producer_data['producer_name'];
        $producer->address = $producer_data['address'];
        $producer->address1 =  $producer_data['address1'];
        $producer->city =  $producer_data['city'];
        $producer->state =  $producer_data['state'];
        $producer->country =  $producer_data['country'];
        $producer->email =  $producer_data['email'];
        $producer->phone = $producer_data['phone'];
        $producer->web =  $producer_data['web'];
        $producer->modified = isset($producer_data['modified']) ? $producer_data['modified'] : null;
        $producer->created = isset($producer_data['created']) ? $producer_data['created'] : null;
        
        return $producer;

    }

    public static function dataFromObject($producer) {
        return [
            'id' => $producer->id,
            'name' => $producer->name,
            'address' => $producer->address,
            'address1' => $producer->address1,
            'city' => $producer->city,
            'state' => $producer->state,
            'country' => $producer->country,
            'email' => $producer->email,
            'phone' => $producer->phone,
            'web' => $producer->web
        ];
    }

    public static function isValid($producer) {
        if(is_array($producer)) {
            return ( isset($producer['email']) && isset($producer['name']));
        }
        return (isset($producer->name) && isset($producer->email));
    }
    
    public static function hasRequiredAttributes($producer) {
        if(is_array($producer)) {
            return ( isset($producer['email']) && isset($producer['producer_name']));
            
        }
        return ( isset($producer->email) && isset($producer->name));
    }

    public static function isValidemail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }


    public static function getPublicData($producer_data) {
        $out = [];
        if(isset($producer_data['id'])) {
            $out['id'] = $producer_data['id'];
        }

        if(isset($producer_data['name'])) {
            $out['name'] = $producer_data['name'];
        }

        if(isset($producer_data['address'])) {
            $out['address'] = $producer_data['address'];
        }
         
        if(isset($producer_data['address1'])) {
            $out['address1'] = $producer_data['address1'];
        }

        if(isset($producer_data['city'])) {
            $out['city'] = $producer_data['city'];
        }

        if(isset($producer_data['state'])) {
            $out['state'] = $producer_data['state'];
        }

        if(isset($producer_data['country'])) {
            $out['country'] = $producer_data['country'];
        }

        if(isset($producer_data['email'])) {
            $out['email'] = $producer_data['email'];
        }

        if(isset($producer_data['phone'])) {
            $out['phone'] = $producer_data['phone'];
        }

        if(isset($producer_data['web'])) {
            $out['web'] = $producer_data['web'];
        }

        return $out;
    }
    


    public static function setFromPublicData($producer_data) {

        $producer = new ProducerModel();
        

        $producer->name =  $producer_data['producer_name'];
        $producer->address =  $producer_data['address'];
        $producer->address1 =  $producer_data['address1'];
        $producer->city =  $producer_data['city'];
        $producer->state =  $producer_data['state'];
        $producer->country =  $producer_data['country'];
        $producer->email =  $producer_data['email'];
        $producer->phone =  $producer_data['phone'];
        $producer->web =  $producer_data['web'];

        return $producer;
    }

}