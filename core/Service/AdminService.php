<?php
namespace Mmanager\Service;

require_once __DIR__ . '/../Controller/UserController.php';
require_once __DIR__ . '/../Model/UserModel.php';
require_once __DIR__ . '/../Model/TempPasswordModel.php';
require_once __DIR__ . '/../Model/ProducerModel.php';
require_once __DIR__ . '/../Model/MaterialModel.php';
require_once __DIR__ . '/../Controller/ProducerController.php';
require_once __DIR__ . '/../Controller/MaterialController.php';
require_once __DIR__ . '/../Controller/DatabaseController.php';
require_once __DIR__ . '/../Controller/MaterialTypeController.php';
require_once __DIR__ . '/../Controller/EmailController.php';
require_once __DIR__ . '/../Controller/LanguageController.php';
require_once __DIR__ . '/../Controller/AdminController.php';
require_once __DIR__ . '/../Controller/AuthController.php';
require_once __DIR__ . '/../Controller/DeliveryTypesController.php';



use Mmanager\Config\Config;

use Mmanager\Model\UserModel;
use Mmanager\Model\ProducerModel;
use Mmanager\Model\MaterialModel;
use Mmanager\Model\TempPasswordModel;

use Mmanager\Controller\UserController;
use Mmanager\Controller\DeliveryTypesController;
use Mmanager\Controller\DatabaseController;
use Mmanager\Controller\ProducerController;
use Mmanager\Controller\MaterialController;
use Mmanager\Controller\MaterialTypeController;
use Mmanager\Controller\AuthController;
use Mmanager\Controller\EmailController;
use Mmanager\Controller\AdminController;
use Mmanager\Controller\LanguageController;

use Jacwright\RestServer\RestException;

class AdminService extends BaseService {




     /**
     *
     * @url GET /profiles
     */
    public function getProfiles()
    {
          $db = DatabaseController::getDB();
          try {
              $auth_users = AuthController::getAuthUsers($db);
          } catch(\Exception $e) {
              throw new RestException($e->getCode(), $e->getMessage());
          }
          return $auth_users;
     }


    /**
     * 
     * @url POST /producer
     */
    public function createProducer($data)
    {
    
         $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }
            static::checkRequirements($data);
        
        try {

            $db = DatabaseController::getDB();
            if(ProducerController::nameExists($db, $data['producer_name'])) {
                throw new \Exception('Producer already exists.', 409);    
            }
            if(ProducerController::emailExists($db, $data['email'])) {
                throw new \Exception('Email already exists.', 409);    
            }
            $producer = ProducerModel::setFromPublicData($data);
            ProducerController::createproducer($db, $producer);
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
    }


    /**
     *
     * @url PUT /producer/$id
     */
    
    public function updateProducer($id, $data)
    {

        $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }

        $db = DatabaseController::getDB();
        try {
            static::checkRequirements($data);
          
            $producer = ProducerModel::objectFromData($data);
            ProducerController::updateproducer($db, $producer,$id);
    
    
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }

    }

     /**
     *
     * @url GET /producers
     */
    public function getProducers()
    {
          try {
            
              $db = DatabaseController::getDB();
              $producers = AuthController::getproducers($db);
          } catch(\Exception $e) {
              throw new RestException($e->getCode(), $e->getMessage());
          }
          return $producers;
     }

      /**
     *
     * @url GET /producer/$id
     */
    public function getProducer($id)
    {
        try {
          
            $db = DatabaseController::getDB();
            $producer = ProducerController::getOneProducer($db, $id);

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
        return $producer;

    }
    private static function checkRequirements($data) {
        if(!ProducerModel::hasRequiredAttributes($data)){
            throw new RestException(400, 'Missing required parameters');
        }
        if(!ProducerModel::isValidemail($data['email'])) {
            throw new RestException(400, 'Email is not an valid');
        }
    }


    /**
     *
     * @url DELETE /producer/$id
     */

    public static function removeProducer($id){



        $db = DatabaseController::getDB();

        $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }

        DatabaseController::executeQuery($db, 'DELETE FROM `producers` WHERE `id` = ?;', [$id], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500); 
        }
        return;

    }



    /**
     *
     * @url GET /materials
     */
    public function getMaterials()
    {
          $db = DatabaseController::getDB();
          try {
              $materials = AuthController::getmaterials($db);
          } catch(\Exception $e) {
              throw new RestException($e->getCode(), $e->getMessage());
          }
          return $materials;
     }

    /**
     *
     * @url GET /material/$id
     */
    public function getMaterial($id)
    {

        $token = AuthController::getAuthTokenFromHeader();

          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }
          
        try { 
            
            $db = DatabaseController::getDB();
            $material = MaterialController::getmaterial($db,$id);

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
        return $material;
    }

    /**
     * 
     * @url POST /material
     */
    public function createMaterial($data)
    {
    
         $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }
            static::checkRequirementsM($data);
        
        try {

            $db = DatabaseController::getDB();
            if(MaterialController::nameExists($db, $data['material_name'])) {
                throw new \Exception('Material already exists.', 409);    
            }
            $material = MaterialModel::setFromPublicData($data);
            MaterialController::creatematerial($db, $material);
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
    }


    /**
     *
     * @url PUT /material/$id
     */
    
    public function updateMaterial($id, $data)
    {

        $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }

        $db = DatabaseController::getDB();
        try {
            static::checkRequirementsM($data);
          
            $material = MaterialModel::objectFromData($data);

            MaterialController::updatematerial($db, $material,$id);
    
    
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }

    }


    /**
     *
     * @url DELETE /material/$id
     */

    public static function removeMaterial($id){



        $db = DatabaseController::getDB();

        $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }

        DatabaseController::executeQuery($db, 'DELETE FROM `materials` WHERE `id` = ?;', [$id], $error);
        if(!empty($error)) {
            throw new \Exception(isset($error[2]) ? $error[2] : 'None', 500); 
        }
        return;

    }



    private static function checkRequirementsM($data) {
        if(!MaterialModel::hasRequiredAttributes($data)){
            throw new RestException(400, 'Missing required parameters');
        }
    }

    /**
     *
     * @url GET /materialtypes
     */
    public function getMattype()
    {
          $db = DatabaseController::getDB();
          try {
              $materialtypes = MaterialTypeController::getmaterialtype($db);
          } catch(\Exception $e) {
              throw new RestException($e->getCode(), $e->getMessage());
          }
          return $materialtypes;
     }

     /**
     *
     * @url GET /countries
     */
    public function getCountries()
    {
          $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }
          $db = DatabaseController::getDB();
          try {
              $countries = AuthController::getCountries($db);
          } catch(\Exception $e) {
              throw new RestException($e->getCode(), $e->getMessage());
          }
          return $countries;
     }

     /**
     *
     * @url GET /deliverytypes
     */
    public function getDeliveryTypes()
    {

         $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }

          try {
              
              $db = DatabaseController::getDB();
              $deliverytypes = DeliveryTypesController::getdeliverytypes($db);
          } catch(\Exception $e) {
              throw new RestException($e->getCode(), $e->getMessage());
          }
          return $deliverytypes;
     }
}