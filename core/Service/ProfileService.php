<?php
namespace Mmanager\Service;

require_once __DIR__ . '/../Controller/UserController.php';
require_once __DIR__ . '/../Model/UserModel.php';
require_once __DIR__ . '/../Model/TempPasswordModel.php';
require_once __DIR__ . '/../Controller/DatabaseController.php';
require_once __DIR__ . '/../Controller/EmailController.php';
require_once __DIR__ . '/../Controller/LanguageController.php';

use Mmanager\Config\Config;

use Mmanager\Model\UserModel;
use Mmanager\Model\TempPasswordModel;

use Mmanager\Controller\UserController;
use Mmanager\Controller\DatabaseController;
use Mmanager\Controller\AuthController;
use Mmanager\Controller\EmailController;
use Mmanager\Controller\LanguageController;

use Jacwright\RestServer\RestException;

class ProfileService extends BaseService {


   /**
   * @noAuth
   * @url GET /profile/activation/$id
   */
  public function activateProfile($id)
  {
    $activation_code = isset($id) ? $id : NULL;
    if($activation_code == NULL) {
        throw new RestException(400, 'Code is not valid!');
    }

    try {
        $db = DatabaseController::getDB();
        $user = UserController::loadUserByCode($db, $activation_code);
        if(UserController::usernameExists($db, $user->username)) {
            throw new \Exception(409, 'User already exists.');    
        }
        UserController::createUser($db, $user, false);
        UserController::removeTempUser($db, $activation_code);
    } catch(\Exception $e) {
        throw new RestException($e->getCode(), $e->getMessage());
    }

    header('Location: ' . Config::ROOT_URL . '/pages/welcome.php' . (isset($_GET['lang']) ? ('?lang=' . $_GET['lang']) : ''));
  }


  /**
   *
   * @url GET /profile
   */
  public function getProfile()
  {
        $db = DatabaseController::getDB();
        try {
            $auth_user = AuthController::getAuthUser($db);
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
        return $auth_user;
   }

  /**
   *
   * @url PUT /profile
   */
  public function updateProfile($data)
  {
    $db = DatabaseController::getDB();
    try {
        
        $old_user = UserModel::getPublicData(AuthController::getAuthUser($db));
        $user = UserModel::getPublicData($data);

        $user = array_merge($old_user,$user);

        UserController::updateUser($db, $user);

    } catch(\Exception $e) {
        throw new RestException($e->getCode(), $e->getMessage());
    }

  }

   /**
   *
   * @url POST /profile/contact
   */
  public function updateContact($data)
  {
    $db = DatabaseController::getDB();
    try {
        ProfileService::checkRequirements($data);

        $old_user = UserModel::getPublicData(AuthController::getAuthUser($db));
        $user = UserModel::getPublicData($data);

        $user = array_merge($old_user,$user);

        UserController::updateUser($db, UserModel::objectFromData($user));

    } catch(\Exception $e) {
        throw new RestException($e->getCode(), $e->getMessage());
    }

  }

  /**
   * 
   * @url PUT /profile/password
   */
  public function changePassword($data)
  {

  }

  /**
   * @noAuth
   * @url POST /profile/password/reset
   */
  public function resetPassword($data)
  {
        // generate token and temp password and send to the user's email

        $username = isset($data['username']) ? $data['username'] : NULL;
        if(!UserModel::isValidUsername($username)) {
            throw new RestException(400, 'Username is not valid');
        }
        $db = DatabaseController::getDB();
        try {
            $user = UserController::loadUserByUsername($db, $username);
            $password = AuthController::generatePin(4);
            $token = AuthController::generateAccessToken();
            
            $tempPassword = new TempPasswordModel();

            $tempPassword->user_id = $user->id;
            $tempPassword->password = $password;
            $tempPassword->token = $token;

            EmailController::resetPasswordMail($username,  $token, $password);
            UserController::createTempPassword($db, $tempPassword);

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }


  }

   /**
   * @noAuth
   * @url GET /profile/password/verify/$id
   */
  public function verifyPassword($id)
  {
        header('Location: ' . Config::ROOT_URL . '/pages/verify-password.php?token=' .$id);
  }

  /**
   * @noAuth
   * @url POST /profile/password/activation
   */
  public function activatePassword($data)
  {
        if(empty($data)) {
            // enable the activation sent with enctype="multipart/form-data"
            $data = $_POST;
        }
        $token = isset($data['token']) ? $data['token'] : NULL;
        $pin = isset($data['pin']) ? $data['pin'] : NULL;
        $password = isset($data['password']) ? $data['password'] : NULL;
        $repeat_password = isset($data['repeat_password']) ? $data['repeat_password'] : NULL;

        if(empty($token) || empty($pin) || empty($password)|| empty($repeat_password)) {
            throw new RestException(400, 'Missing required parameters');
        }
        if ($password !== $repeat_password) {
            throw new RestException(400, 'Password mismatch');
        }

        try {
            $db = DatabaseController::getDB();

            $temp_password = UserController::findTempPasswordByToken($db, $token, $pin);
            if(!AuthController::verifyPassword($pin, $temp_password->password)) {
                throw new RestException(400, 'Wrong pin');
            }
            UserController::removeTempPassword($db,$token);
            UserController::updateUserPassword($db,$temp_password->user_id, $password);

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }

        header('Location: ' . Config::ROOT_URL . '/pages/password-confirm.php'  . (isset($_POST['lang']) ? ('?lang=' . $_POST['lang']) : ''));

  }

    /**
     *
     * @url DELETE /profile
     */
    public function deleteProfile($id) 
    {
        $db = DatabaseController::getDB();
        try {
            
            UserController::deleteUser($db, $id);

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }

    }


   /**
   * Get new auth token
   *
   * @url GET /profile/token
   */
    public function token()
    {
        try {

            $db = DatabaseController::getDB();
            $user = AuthController::getAuthUser($db);
            return UserController::createUserToken($db, $user['id']);

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
    }

    

}