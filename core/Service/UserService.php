<?php
namespace Mmanager\Service;

require_once __DIR__ . '/../Controller/DatabaseController.php';
require_once __DIR__ . '/../Controller/UserController.php';
require_once __DIR__ . '/../Controller/EmailController.php';
require_once __DIR__ . '/../Model/TempUserModel.php';
require_once __DIR__ . '/../Model/UserModel.php';

use Mmanager\Controller\DatabaseController;
use Mmanager\Model\UserModel;
use Mmanager\Model\TempUserModel;
use Mmanager\Controller\UserController;
use Mmanager\Controller\AuthController;
use Mmanager\Controller\EmailController;

use Jacwright\RestServer\RestException;

class UserService extends BaseService {

    
    /**
     * @noAuth
     * @url POST /user
     */
    public function createUser($data)
    {
    
        UserService::checkRequirements($data);
        
        try {
            $db = DatabaseController::getDB();
            if(UserController::usernameExists($db, $data['username'])) {
                throw new \Exception('User already exists.', 409);    
            }
            $user = TempUserModel::setFromPublicData($data);
            $user->activation_code = base64_encode(rand(1, 1000000) . '-' . time());
            UserController::createTempUser($db, $user);
            EmailController::welcomeMail($user->username, $user->activation_code);
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
    }

    /**
     *
     * @url GET /user/$id
     */
    public function getUser($id)
    {
        $db = DatabaseController::getDB();
        try {
            $user = UserModel::getPublicData(UserModel::dataFromObject(UserController::loadUserById($db, $id)));

        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }
        return $user;

    }

    /**
     *
     * @url PUT /user/$id
     */
    public function updateUser($id, $data)
    {
        $db = DatabaseController::getDB();
        try {
            ProfileService::checkRequirements($data);
    
            $old_user = UserModel::getPublicData(UserController::loadUserById($db, $id));
            $user = UserModel::getPublicData($data);
    
            $user = array_merge($old_user,$user);
    
            UserController::updateUser($db, UserModel::objectFromData($user));
    
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }

    }

    private static function checkRequirements($data) {
        if(!UserModel::hasRequiredAttributes($data)){
            throw new RestException(400, 'Missing required parameters');
        }
        if(!UserModel::isValidUsername($data['username'])) {
            throw new RestException(400, 'Username is not an email');
        }
    }

    /**
     *
     * @url DELETE /user/$id
     */
    public function deleteUser($data)
    {

    }






}

?>