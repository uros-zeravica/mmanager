<?php

namespace Mmanager\Service;

require_once __DIR__ . '/../Controller/DatabaseController.php';
require_once __DIR__ . '/../Controller/AuthController.php';

use Mmanager\Controller\DatabaseController;
use Mmanager\Controller\AuthController;

class BaseService {

    public function authorize() {
        try {
            return AuthController::isAutorised(DatabaseController::getDB());
        } catch(\Exception $e) {
            return false;
        }
    }

   
}
?>
