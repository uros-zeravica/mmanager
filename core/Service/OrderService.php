<?php
namespace Mmanager\Service;

require_once __DIR__ . '/../Controller/DatabaseController.php';
require_once __DIR__ . '/../Controller/AuthController.php';
require_once __DIR__ . '/../Controller/OrderController.php';
require_once __DIR__ . '/../Model/OrderModel.php';
require_once __DIR__ . '/../Model/OrderItemsModel.php';

use Mmanager\Config\Config;

use Mmanager\Controller\OrderController;
use Mmanager\Controller\DatabaseController;
use Mmanager\Controller\AuthController;
use Mmanager\Model\OrderModel;
use Mmanager\Model\OrderItemsModel;


use Jacwright\RestServer\RestException;

class OrderService extends BaseService {

        

    /**
     * 
     * @url POST /order
     */
    
    public function createOrder($data)
    {
        
         $token = AuthController::getAuthTokenFromHeader();
          if(empty($token)) {
            throw new \Exception('Unauthorized', 401);
          }
        $db = DatabaseController::getDB();
        $user = AuthController::getUserByAccessToken($db, $token);
        if(empty($user) || !isset($user['user_id'])) {
            throw new \Exception('Unauthorized', 401);
        }
        $data['user_id'] = $user['user_id'];
        
        try {


            $orderItems = OrderItemsModel::getItemsData($data);
            return $orderItems;
            $order = OrderModel::setFromPublicData($data);
            OrderController::createOrder($db, $order);
            /*OrderController::createOrderItems($db,$data);
            OrderController::createOrderItemsElements($db,$data);
*/
        } catch(\Exception $e) {
            throw new RestException($e->getCode(), $e->getMessage());
        }

    }
   
    

}


?>