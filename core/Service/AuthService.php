<?php

namespace Mmanager\Service;

require_once __DIR__ . '/../Controller/UserController.php';
require_once __DIR__ . '/../Model/UserModel.php';
require_once __DIR__ . '/../Model/ProducerModel.php';

use Mmanager\Model\UserModel;
use Mmanager\Model\ProducerModel;
use Mmanager\Model\UserStatus;
use Mmanager\Controller\AuthController;
use Mmanager\Controller\DatabaseController;
use Mmanager\Controller\UserController;
use Mmanager\Service\BaseService;

use Jacwright\RestServer\RestException;

class AuthService extends BaseService {

  /**
   * @noAuth
   * @url POST /login
   */
  public function login($data)
  {

        $username = isset($data['username']) ? $data['username'] : NULL;
        $password = isset($data['password']) ? $data['password'] : NULL;

        if(empty($username) || empty($password)) {
            throw new RestException(400, 'Missing required parameters');
        }

        $db = DatabaseController::getDB();

        $row = DatabaseController::fetchRow($db,"SELECT `id`, `username`, `password`, `status` FROM `user` WHERE username=? AND status = ?", [$username, UserStatus::ACTIVE], $error);
        $admin = DatabaseController::fetchRow($db,"SELECT `id`, `username`, `password`, `status` FROM `user` WHERE username=? AND status = ?", [$username, UserStatus::INACTIVE], $error);

        if(!empty($error)) {
            throw new RestException(500, isset($error[2]) ? $error[2] : 'None');
        }
        if(empty($row) && empty($admin)) {
            throw new RestException(400, 'Wrong username');
        }

        if(!AuthController::verifyPassword($password, $row['password']) && !AuthController::verifyPassword($password, $admin['password'])) {
            throw new RestException(400, 'Wrong password');
        }

        
        if (AuthController::verifyPassword($password, $row['password'])) {
         
            return AuthController::setAccessToken($db, $row['id'], '',$row['status']);
        }

        if (AuthController::verifyPassword($password, $admin['password'])) {
         
            return AuthController::setAccessToken($db, $admin['id'], '',$admin['status']);
        }

   }

   /**
   *
   *
   * @url POST /logout
   */
  public function logout($data)
  {
        $token = AuthController::getAuthTokenFromHeader();
        if(empty($token)) {
            throw new RestException(401, 'Unauthorized');
        }
        
        $db = DatabaseController::getDB();
        if(!AuthController::unsetAccessToken($db, $token)) {
            throw new RestException(500, 'Internal error');
        }
        
  }

  /**
   * Checkin user into some organisation; This bounds the user's access token to the selected orga
   *
   * @url POST /checkin
   */
  public function checkin($data)
  {
        $db = DatabaseController::getDB();
        
        
  }


   
}
?>
