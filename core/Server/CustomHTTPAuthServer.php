<?php
namespace Mmanager\Server;

class CustomHTTPAuthServer extends \Jacwright\RestServer\Auth\HTTPAuthServer {

    public function unauthorized($classObj) {
		throw new \Jacwright\RestServer\RestException(401, "Unauthorized");
    }
    
}