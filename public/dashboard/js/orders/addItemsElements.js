	
		function addItemsElements(){

		var button = document.getElementById('addElements');

          
          var orderItemElements = [];
          button.addEventListener('click',function(){

    
          var width = document.getElementById('width_ordItm_elem').value;
          var height = document.getElementById('height_ordItm_elem').value;
          var direction = document.getElementById('direction_ordItm_elem').value;
          var quantity_el = document.getElementById('quantity_ordItm_elem').value;
          var iterator = localStorage.getItem('elements_iterator');

          var ItemElement = {'el_num':iterator,
          "attributes":{
            "width":width,
            "height":height,
            "direction":direction,
            "quantity_el":quantity_el
          }
        }
          orderItemElements.push(ItemElement);

          localStorage.setItem('orderItemElements',JSON.stringify(orderItemElements));
          orderItemElements = JSON.parse(localStorage.getItem('orderItemElements'));


          

          var div_element = '<div id="'+iterator+'"class="list-group-item list-group-item-action">\
          Element '+iterator+':<br>\
          <strong>Width:&nbsp;&nbsp;</strong><span id="width" value="'+width+'">'+width+'</span>\
          <strong>Height:&nbsp;&nbsp;</strong><span id="height" value="'+height+'">'+height+'</span><br>\
          <strong>Direction:&nbsp;&nbsp;</strong><span id="direction" value="'+direction+'">'+direction+'</span>\
          <strong>Quantity:&nbsp;&nbsp;</strong><span id="quantity_el" value="'+quantity_el+'">'+quantity_el+'</span>\
          <button class="btn btn-primary btn-sm" onclick="deleteItemElements(this)" style="float: right; padding: 7px;">\
          <i title="Delete" class="fas fa-minus-circle"></i>\
          </button><button class="btn btn-primary btn-sm"  style="float: right; padding: 7px;" onclick="editItemElements(this)" \
          data-toggle="modal" data-target="#UpdateOrderItemElementsModal">\
          <i title="Edit" class="fa fa-pencil-square-o"></i></button></div>';

          iterator++;

          //Add divs to DOM
          $('#listItemElements').append(div_element);

          //Close Modal
         $("#CreateItemElements .close").click();

         //Increment iterator
          localStorage.setItem('elements_iterator',iterator);
          });

          //Pagination
	}