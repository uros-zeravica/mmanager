
	function getMatNamesEdit(){


    	var access_token = localStorage.getItem("access_token");

	

		var xhr1 = new XMLHttpRequest();
        var url1 = "/materials";
          xhr1.open("GET", url1, true);
          xhr1.setRequestHeader("Content-type", "application/json");
          xhr1.setRequestHeader("Accept","application/json");
          xhr1.setRequestHeader("Accept-Language","EN");
          xhr1.setRequestHeader("Authorization","Bearer "+access_token);

			xhr1.onload = function() {
			  if (xhr1.status === 200) {

			  	var dropdown = document.getElementById('edit_materialname');
				var defaultOption = document.createElement('option');
				var text_default = document.createTextNode('Add Materials');
				defaultOption.appendChild(text_default);
				dropdown.appendChild(defaultOption);
				dropdown.selectedIndex = "0";
			    const data = JSON.parse(xhr1.response);
			    var option;
			    for (var i = 0; i < data.length; i++) {
			      option = document.createElement('option');
			      var text = document.createTextNode(data[i].material_name);
			      option.appendChild(text);
			      option.setAttribute('value',data[i].id);
			      dropdown.appendChild(option);
			    }
			   } else {
			    	alert('Reached the server, but it returned an error');
			  }   
			}

			xhr1.onerror = function() {
			  console.error('An error occurred fetching the JSON from ' + url);
			};

			xhr1.send();

		}
