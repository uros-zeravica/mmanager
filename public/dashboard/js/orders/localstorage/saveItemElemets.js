function saveItemElemets(){


	var button = document.getElementById('saveItemElemets');

	button.addEventListener('click', function(){

		var new_width = document.getElementById('edit_width_ordItm_elem').value;
		var new_height = document.getElementById('edit_height_ordItm_elem').value;
		var new_direction = document.getElementById('edit_direction_ordItm_elem').value;
		var new_quantity = document.getElementById('edit_quantity_ordItm_elem').value;

		var right_item = JSON.parse(localStorage.getItem('orderItemElements'));
		var edit_elem = JSON.parse(localStorage.getItem('edit_elem'));

		var right_div = $("#listItemElements").children("#"+edit_elem);


	     for (var i = 0; i < right_item.length; i++) {
	     
		if (edit_elem == right_item[i].el_num) {

			right_item[i].attributes.width  = document.getElementById('edit_width_ordItm_elem').value;
			right_item[i].attributes.height = document.getElementById('edit_height_ordItm_elem').value;
			right_item[i].attributes.direction = document.getElementById('edit_direction_ordItm_elem').value;
			right_item[i].attributes.quantity_el = document.getElementById('edit_quantity_ordItm_elem').value;

			// Change Div
			
			right_div.children("#width")[0].innerHTML = right_item[i].attributes.width;
			right_div.children("#width")[0].setAttribute('value',right_item[i].attributes.width);

			right_div.children("#height")[0].innerHTML = right_item[i].attributes.height;
			right_div.children("#height")[0].setAttribute('value',right_item[i].attributes.height);

			right_div.children("#direction")[0].innerHTML = right_item[i].attributes.direction;
			right_div.children("#direction")[0].setAttribute('value',right_item[i].attributes.direction);

			right_div.children("#quantity_el")[0].innerHTML = right_item[i].attributes.quantity_el;
			right_div.children("#quantity_el")[0].setAttribute('value',right_item[i].attributes.quantity_el);


			}
			
		}

		
	    localStorage.setItem('orderItemElements',JSON.stringify(right_item));
         $("#UpdateOrderItemElementsModal .close").click();


		});


}