	
		function addItems(){


		var button = document.getElementById('addItem');
          
          
          button.addEventListener('click',function(){

          //Add MatItems to localstorage

          //Clear Div ItemElements
          $( "#listItemElements" ).empty();  


          var option = $("#materialname option:selected").html();
          var id = $("#materialname option:selected").val();
          var order_elems = JSON.parse(localStorage.getItem('orderItemElements'));
          var order_obj = localStorage.getItem(('order_obj'));
          if (order_obj == "") {
            order_obj = [];
          }
          else
          {
            order_obj = JSON.parse(localStorage.getItem('order_obj'));
          }

          var items_obj = {

            "material_id":id ,
            "orderElements":order_elems
          };
          order_obj.push(items_obj)
          localStorage.setItem('order_obj',JSON.stringify(order_obj));

          var div = '<div id='+id+' class="list-group-item list-group-item-action">'+option+'<button class="btn btn-primary btn-sm" onclick="deleteItem(this)" \
          style="float: right; padding: 7px;">\
          <i title="Delete" class="fas fa-minus-circle"></i>\
              </button><button class="btn btn-primary btn-sm"  style="float: right; padding: 7px;" onclick="editItem(this)" data-toggle="modal"  \
              data-target="#UpdateOrderItemModal">\
              <i title="Edit" class="fa fa-pencil-square-o"></i></button></div>';

          //Add div to DOM
          $('#listShow').prepend(div);

          //Close Modal
         $("#CreateOrderModal .close").click();

         //Reset iterator
          localStorage.setItem('elements_iterator',1);
          localStorage.removeItem('orderItemElements');
          });
          //Pagination
	}