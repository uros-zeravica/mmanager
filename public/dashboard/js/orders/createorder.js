     
    var createproducer = document.getElementById('create-order');
    createproducer.addEventListener('click', function(e){
    
      
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/dashboard/pages/createorder.html", true);

      xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){

          var page = xhr.responseText;
          // Set order_obj
          localStorage.setItem('order_obj',[]);

          var div = document.getElementById('fill');
          div.innerHTML = page;
          getMaterialsName();
          addItems();
          getDeliveryTypes();
          addItemsElements();
          localStorage.setItem('elements_iterator',1);
          $( function() {
            $( "#requested_date" ).datepicker();
          });    
          httpcreateO();

        }else{ 

                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
    }
    e.preventDefault();

      xhr.send();
    });

  