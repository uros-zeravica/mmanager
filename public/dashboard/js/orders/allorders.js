	
		var allorders = document.getElementById('allorders');
		allorders.addEventListener('click', function(e){
		
			
		  var xhr = new XMLHttpRequest();
		  xhr.open("GET", "/dashboard/pages/all-orders.html", true);

		  xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){

		    	var page = xhr.responseText;
		    	var div = document.getElementById('fill');
		    	div.innerHTML = page;
		    	//getAllProducers();
		    	//getCountries();
		    	redirectCreateO();
		    }else{ 

                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
		}
		e.preventDefault();

		  xhr.send();
		});

	function redirectCreateO(){
		
		var createproducer = document.getElementById('createOrder');
		createproducer.addEventListener('click', function(e){
		
			
		  var xhr = new XMLHttpRequest();
		  xhr.open("GET", "/dashboard/pages/createorder.html", true);

		  xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){

		    	var page = xhr.responseText;
         		 localStorage.setItem('order_obj',[]);
		    	var div = document.getElementById('fill');
		    	div.innerHTML = page;
		    	//getCountries();
		    	//httpcreate();
		    	getMaterialsName();
		    	addItems();
		    	getDeliveryTypes();
		    	addItemsElements();
		    	httpcreateO();
		    	$( function() {
            	$( "#requested_date" ).datepicker();
          		});
          		localStorage.setItem('elements_iterator',1);
		    	
		    }else{ 

                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
		}
		e.preventDefault();

		  xhr.send();
		});       
	}                      	