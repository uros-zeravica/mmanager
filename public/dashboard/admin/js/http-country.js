
	function getCountries(){

	let dropdown = document.getElementById('country');
    var access_token = localStorage.getItem("access_token");
	let defaultOption = document.createElement('option');
	let text_default = document.createTextNode('Choose Country');
	defaultOption.appendChild(text_default);
	dropdown.appendChild(defaultOption);
	dropdown.selectedIndex = "0";
	

	const url = '/countries';

	const request = new XMLHttpRequest();
	request.open('GET', url, true);
	request.setRequestHeader("Content-type", "application/json");
    request.setRequestHeader("Accept","application/json");
    request.setRequestHeader("Accept-Language","EN");
    request.setRequestHeader("Authorization","Bearer "+access_token);

	request.onload = function() {
	  if (request.status === 200) {
	    const data = JSON.parse(request.responseText);
	    let option;
	    for (let i = 0; i < data.length; i++) {
	      option = document.createElement('option');
	      text = document.createTextNode(data[i].name);
	      option.appendChild(text);
	      option.setAttribute('value',data[i].id);
	      dropdown.appendChild(option);
	    }
	   } else {
	    // Reached the server, but it returned an error
	  }   
	}

	request.onerror = function() {
	  console.error('An error occurred fetching the JSON from ' + url);
	};

	request.send();

}
