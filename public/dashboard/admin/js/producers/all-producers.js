	
		var allproducers = document.getElementById('allproducers');
		allproducers.addEventListener('click', function(e){
		
			
		  var xhr = new XMLHttpRequest();
		  xhr.open("GET", "/dashboard/admin/pages/all-producers.html", true);

		  xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){

		    	var page = xhr.responseText;
		    	var div = document.getElementById('fill');
		    	div.innerHTML = page;
		    	getAllProducers();
		    	getCountries();
		    	redirectCreateP();
		    }else{ 

                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
		}
		e.preventDefault();

		  xhr.send();
		});

	function redirectCreateP(){
		
		var createproducer = document.getElementById('createproducer');
		createproducer.addEventListener('click', function(e){
		
			
		  var xhr = new XMLHttpRequest();
		  xhr.open("GET", "/dashboard/admin/pages/createproducer.html", true);

		  xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){

		    	var page = xhr.responseText;
		    	var div = document.getElementById('fill');
		    	div.innerHTML = page;
		    	getCountries();
		    	httpcreate();

		    }else{ 

                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
		}
		e.preventDefault();

		  xhr.send();
		});       
	}                      	