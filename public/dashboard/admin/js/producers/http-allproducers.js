      
          function getAllProducers(){
        

          var access_token = localStorage.getItem("access_token");
          var user_status = localStorage.getItem("user_status");


          var xhr1 = new XMLHttpRequest();
          var url1 = "/producers";
          xhr1.open("GET", url1, true);
          xhr1.setRequestHeader("Content-type", "application/json");
          xhr1.setRequestHeader("Accept","application/json");
          xhr1.setRequestHeader("Accept-Language","EN");
          xhr1.setRequestHeader("Authorization","Bearer "+access_token);

          xhr1.onreadystatechange = function(){

           if(xhr1.readyState === xhr1.DONE){
            if(xhr1.status === 200){

              var res = xhr1.response;
              res = JSON.parse(res);
              for (var i = 0; i < res.length; i++) {
                
              
              var id = res[i].id;
              var name = res[i].producer_name;
              var address = res[i].address;
              var address1 = res[i].address1;
              var city = res[i].city;
              var state = res[i].state;
              var country_id = res[i].country;
              var email = res[i].email;
              var phone = res[i].phone;
              var web = res[i].web;
              var created = res[i].created;
              var countryname = res[i].name;



              //string for class
      
              var btn_class = "pd-setting-ed";
              var fa = 'fa';
              var pencil = 'fa-pencil-square-o';
              var trash = 'fa-trash';
              var del_user = 'del-producer';
              var edit_user = 'edit-producer';
              
              //Adding to Dom
              
              var parent = document.getElementById('table');
              var tr = document.createElement("tr");
              var td = document.createElement("td");
              var td1 = document.createElement("td");
              var td2 = document.createElement("td");
              var td3 = document.createElement("td");
              var td4 = document.createElement("td");
              var td5 = document.createElement("td");
              var td6 = document.createElement("td");
              var td7 = document.createElement("td");
              var td8 = document.createElement("td");
              var td9 = document.createElement("td");
              var td10 = document.createElement("td");
              var td11 = document.createElement("td");
              var td12 = document.createElement("td");
              var a = document.createElement("a");

              var button1 = document.createElement("button");
              var button2 = document.createElement("button");
              var icon1 = document.createElement("i");
              var icon2 = document.createElement("i");

              a.setAttribute('href',web);

              icon1.setAttribute('title','Edit');
              icon2.setAttribute('title','Delete');

              icon1.classList.add(fa,pencil);
              icon2.classList.add(fa,trash);

              tr.setAttribute('id',id+""+id);

              button1.setAttribute('id',id);
              button2.setAttribute('id',id);

              button1.setAttribute('onclick','editProducer(this)');
              button1.setAttribute('data-toggle','modal');
              button1.setAttribute('data-target','#myModal');
              button2.setAttribute('onclick','deleteProducer(this)');
              
             
              button1.classList.add(btn_class);
              button2.classList.add(btn_class);      
              button1.classList.add(edit_user);
              button2.classList.add(del_user);

              var text_id = document.createTextNode(id);
              var text_name = document.createTextNode(name);
              var text_address = document.createTextNode(address);
              var text_address1 = document.createTextNode(address1);
              var text_city = document.createTextNode(city);
              var text_state = document.createTextNode(state);
              var text_country = document.createTextNode(countryname);
              var text_email = document.createTextNode(email);
              var text_phone = document.createTextNode(phone);
              var text_web = document.createTextNode(web);
              var text_created = document.createTextNode(created);


              var text_created = document.createTextNode(created);
              var text_id = document.createTextNode(id);
              
              a.appendChild(text_web);
              button1.appendChild(icon1);
              button2.appendChild(icon2);

              td11.appendChild(text_id);
              td.appendChild(text_name);
              td1.appendChild(text_address);
              td2.appendChild(text_address1);
              td3.appendChild(text_city);
              td4.appendChild(text_state);
              td6.appendChild(text_country);
              td7.appendChild(text_email);
              td8.appendChild(text_phone);
              td9.appendChild(a);

              td10.appendChild(button1);
              td10.appendChild(button2);

              td12.appendChild(text_created);


              parent.appendChild(tr);
              tr.appendChild(td11);
              tr.appendChild(td);
              tr.appendChild(td1);
              tr.appendChild(td2);
              tr.appendChild(td3);
              tr.appendChild(td4);
              tr.appendChild(td6);
              tr.appendChild(td7);
              tr.appendChild(td8);
              tr.appendChild(td9);
              tr.appendChild(td12);
              tr.appendChild(td10);
              $(document).ready(function () {
              $('#mytable').DataTable();
              $('.dataTables_length').addClass('bs-select');
            });

                
              }
            }
               
            else{ 

                    var res = xhr1.response;
                    response = JSON.parse(res);
                    var message = response.error.message;                    
                     }
            }

        }
              xhr1.send();
      
  }