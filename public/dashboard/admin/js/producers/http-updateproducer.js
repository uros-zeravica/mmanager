
		function httpedit(){

        var button = document.getElementById('submit');
        button.addEventListener('click',function(){

        var id_producer = localStorage.getItem('id');
        var name = document.getElementById('name').value;
        var address = document.getElementById('address').value;
        var address1 = document.getElementById('address1').value;
        var city = document.getElementById('city').value;
        var state = document.getElementById('state').value;
        var country = document.getElementById('country').value;
        var email = document.getElementById('email').value;
        var phone = document.getElementById('phone').value;
        var website = document.getElementById('website').value;

		    var access_token = localStorage.getItem("access_token");




        
        var xhr = new XMLHttpRequest();
        var url = "/producer/"+id_producer;
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader("Accept","application/json");
        xhr.setRequestHeader("Accept-Language","EN");
        xhr.setRequestHeader("Authorization","Bearer "+access_token);
        xhr.onreadystatechange = function(){
    
          if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200) {
                

              var x = document.getElementsByTagName("BODY")[0];
               x.style.overflow = 'scroll';  
              var countryname = localStorage.getItem('countryname');
              var tr = document.getElementById(id_producer+""+id_producer);
             

              td0 = tr.cells[0];
              td1 = tr.cells[1];
              td2 = tr.cells[2];
              td3 = tr.cells[3];
              td4 = tr.cells[4];
              td5 = tr.cells[5];
              td6 = tr.cells[6];
              td7 = tr.cells[7];
              td8 = tr.cells[8];
              td9 = tr.cells[9];

              td0.innerHTML = id_producer;
              td1.innerHTML = name;
              td2.innerHTML = address;
              td3.innerHTML = address1;
              td4.innerHTML = city;
              td5.innerHTML = state;
              if (countryname != null) {
              td6.innerHTML = countryname;
              }
              td7.innerHTML = email;
              td8.innerHTML = phone;
              td9.innerHTML = '<a href="'+website+'">'+website+'</a>';
           
              localStorage.removeItem('id');

              $("#myModal").modal('hide');
             
              
              
              
          }
            
            else{  
                
                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(res); 
        }
          }
      }

        var data = JSON.stringify({
        	"producer_name": name, "address": address, "address1": address1, "state": state,
          "country": country,"email": email, "phone": phone, "web": website, "city": city,
        });
        xhr.send(data);

    });
}
     