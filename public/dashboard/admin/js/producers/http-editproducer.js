  


  function editProducer(e){


    var id = e.id;
    localStorage.setItem('id',id); 
          
          

          getProducer();
          httpedit();

  }  

  function getProducer(){

    var access_token = localStorage.getItem("access_token");
    var id_producer = localStorage.getItem('id');

      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/producer/"+id_producer, true);
      xhr.setRequestHeader("Content-type", "application/json");
      xhr.setRequestHeader("Accept","application/json");
      xhr.setRequestHeader("Accept-Language","EN");
      xhr.setRequestHeader("Authorization","Bearer "+access_token);

      xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){


              var res = xhr.response;
              res = JSON.parse(res);
            
              var id = res.id;
              var name = res.producer_name;
              var address = res.address;
              var address1 = res.address1;
              var city = res.city;
              var state = res.state;
              var country_id = res.country;
              var email = res.email;
              var phone = res.phone;
              var web = res.web;
              var countryname = res.name;

              localStorage.removeItem('countryname');
              var select = document.getElementById("country");
              select.onclick = function(){
                 
                  var options = this.getElementsByTagName("option");
                  var countryname = options[this.selectedIndex].innerHTML;  
                  localStorage.setItem('countryname',countryname);
              };  


              

              document.getElementById('name').value = name;
              document.getElementById('address').value = address;
              document.getElementById('address1').value = address1;
              document.getElementById('city').value = city;
              document.getElementById('state').value = state;
              document.getElementById('country').value = country_id;
              document.getElementById('email').value = email;
              document.getElementById('phone').value = phone;
              document.getElementById('website').value = web;


        }else{ 

                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
      }

      xhr.send();
    
    
    
  }
              