      
          function getUsers(){

          var getUsers = document.getElementById('getUsers');


          getUsers.addEventListener('click',function(){
        
          

          var access_token = localStorage.getItem("access_token");
          var user_status = localStorage.getItem("user_status");


          var xhr1 = new XMLHttpRequest();
          var url1 = "/profiles";
          xhr1.open("GET", url1, true);
          xhr1.setRequestHeader("Content-type", "application/json");
          xhr1.setRequestHeader("Accept","application/json");
          xhr1.setRequestHeader("Accept-Language","EN");
          xhr1.setRequestHeader("Authorization","Bearer "+access_token);

          xhr1.onreadystatechange = function(){

           if(xhr1.readyState === xhr1.DONE){
            if(xhr1.status === 200){

              var res = xhr1.response;
              res = JSON.parse(res);
              for (var i = 0; i < res.length; i++) {
                
              
              var id = res[i].id;
              var email = res[i].username;
              var user_status = res[i].status;
              var first_name = res[i].first_name;
              var last_name = res[i].last_name;
              var created = res[i].created;

              //string for class
      
              var btn_class = "pd-setting-ed";
              var fa = 'fa';
              var pencil = 'fa-pencil-square-o';
              var trash = 'fa-trash-o';
              var del_user = 'del-user';
              var edit_user = 'edit-user';
              
              //Adding to Dom
              
              var parent = document.getElementById('table');
              var tr = document.createElement("tr");
              var td = document.createElement("td");
              var td1 = document.createElement("td");
              var td2 = document.createElement("td");
              var td3 = document.createElement("td");
              var td4 = document.createElement("td");
              var td5 = document.createElement("td");
              var button1 = document.createElement("button");
              var button2 = document.createElement("button");
              var icon1 = document.createElement("i");
              var icon2 = document.createElement("i");


              icon1.setAttribute('title','Edit');
              icon2.setAttribute('title','Delete');

              icon1.classList.add(fa,pencil);
              icon2.classList.add(fa,trash);

              button1.setAttribute('id',id);
              button2.setAttribute('id',id);
              
             
              button1.classList.add(btn_class);
              button2.classList.add(btn_class);      
              button1.classList.add(edit_user);
              button2.classList.add(del_user);

              var text_name = document.createTextNode(first_name + " " + last_name);
              var text_status = document.createTextNode(user_status);
              var text_email = document.createTextNode(email);
              var text_created = document.createTextNode(created);
              var text_id = document.createTextNode(id);
              

              button1.appendChild(icon1);
              button2.appendChild(icon2);

              td5.appendChild(text_id);
              td.appendChild(text_name);
              td1.appendChild(text_email);
              td2.appendChild(text_status);
              td3.appendChild(text_created);
              td4.appendChild(button1);
              td4.appendChild(button2);
              parent.appendChild(tr);
              tr.appendChild(td5);
              tr.appendChild(td);
              tr.appendChild(td1);
              tr.appendChild(td2);
              tr.appendChild(td3);
              tr.appendChild(td4);
              document.getElementById('getUsers').style.display = "none";
                
              }
            }
               
            else{ 

                    var res = xhr.response;
                    response = JSON.parse(res);
                    alert(response);
                    var message = response.error.message;
                    document.getElementById("error").style.display = "block";
                    document.getElementById('error').innerHTML = message;
                    
                     }
            }

        }
              xhr1.send();

      });
      
  }