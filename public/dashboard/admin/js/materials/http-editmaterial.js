  


  function editmaterial(e){


    var id_m = e.id;
    localStorage.setItem('id_m',id_m); 
  
          getMaterial();
          httpeditM();

  }  

  function getMaterial(){

    var access_token = localStorage.getItem("access_token");
    var id_material = localStorage.getItem('id_m');

      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/material/"+id_material, true);
      xhr.setRequestHeader("Content-type", "application/json");
      xhr.setRequestHeader("Accept","application/json");
      xhr.setRequestHeader("Accept-Language","EN");
      xhr.setRequestHeader("Authorization","Bearer "+access_token);

      xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){


              var res = xhr.response;
              res = JSON.parse(res);
            
              var id_m = res.id;
              var material_name = res.material_name;
              var image = res.image_url;
              var producer_id = res.producer_id;
              var mtype_id = res.type_id;
              var producer_name = res.producer_name;
              var materialtype_name = res.materialtype_name;
              var original_width = res.original_width;
              var original_height = res.original_height;
              var original_depth = res.original_depth;
              var code = res.code;
              var descripton = res.description_m;
              

              localStorage.removeItem('producer_name');
              localStorage.removeItem('mattype_name');
              var selectpname = document.getElementById("producername");
              selectpname.onclick = function(){
                 
                  var options = this.getElementsByTagName("option");
                  var producer_name = options[this.selectedIndex].innerHTML;  
                  localStorage.setItem('producer_name',producer_name);
              };
              var selectmtypename = document.getElementById("material-type");
              selectmtypename.onclick = function(){
                 
                  var options = this.getElementsByTagName("option");
                  var material_type_name = options[this.selectedIndex].innerHTML;  
                  localStorage.setItem('material_type_name',material_type_name);
              };


              document.getElementById('image_m').value = image;
              document.getElementById('m_name').value = material_name;
              var drop_names = document.getElementById("producername");
              drop_names.value = producer_id;
              drop_type_id = document.getElementById("material-type");
              drop_type_id.value = mtype_id;
              document.getElementById('o_width').value = original_width;
              document.getElementById('o_height').value = original_height;
              document.getElementById('o_depth').value = original_depth;
              document.getElementById('m_code').value = code;
              document.getElementById('descripton_m').value = descripton;

        }else{ 

                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
              }
          }
      }

      xhr.send();
    
    
    
  }
              