
		function httpeditM(){

        var button = document.getElementById('submit_m');
        button.addEventListener('click',function(){

        var m_name = document.getElementById('m_name').value;
        var producername = document.getElementById('producername').value;
        var material_type = document.getElementById('material-type').value;
        var o_width = document.getElementById('o_width').value;
        var o_height = document.getElementById('o_height').value;
        var o_depth = document.getElementById('o_depth').value;
        var m_code = document.getElementById('m_code').value;
        var descripton_m = document.getElementById('descripton_m').value;
        var image_m = document.getElementById('image_m').value;

        var access_token = localStorage.getItem("access_token");
        var id_material = localStorage.getItem('id_m');





        
        var xhr = new XMLHttpRequest();
        var url = "/material/"+id_material;
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader("Accept","application/json");
        xhr.setRequestHeader("Accept-Language","EN");
        xhr.setRequestHeader("Authorization","Bearer "+access_token);
        xhr.onreadystatechange = function(){
    
          if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200) {
              
              var x = document.getElementsByTagName("BODY")[0];
               x.style.overflow = 'scroll';  
              var tr = document.getElementById(id_material+""+id_material);
              var producer_name = localStorage.getItem('producer_name');
              var material_type_name = localStorage.getItem('material_type_name');


              td0 = tr.cells[0];
              td1 = tr.cells[1];
              td2 = tr.cells[2];
              td3 = tr.cells[3];
              td4 = tr.cells[4];
              td5 = tr.cells[5];
              td6 = tr.cells[6];
              td7 = tr.cells[7];
              td8 = tr.cells[8];
              td9 = tr.cells[9];


              td0.innerHTML = id_material;
              td1.innerHTML = '<img src="'+image_m+'" width="70px">';
              td2.innerHTML = m_name;
              if (producer_name != null) {
              td3.innerHTML = producer_name;
              }
              if (material_type_name != null) {
              td4.innerHTML = material_type_name;
              } 
              td5.innerHTML = o_width;
              td6.innerHTML = o_height;
              td7.innerHTML = o_depth;
              td8.innerHTML = m_code;
              td9.innerHTML = descripton_m;

              localStorage.removeItem('id_m');
              
              $("#myModalMaterials").modal('hide');
              
          }
            
            else{  
                
                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;

                  alert(message); 
          }
        }
      }

        var data = JSON.stringify({
          "image_url": image_m, "material_name": m_name, "producer_id": producername, "type_id": material_type,
          "original_width": o_width,"original_height": o_height, "original_depth": o_depth, "code": m_code, 
          "description_m": descripton_m,
        });
        xhr.send(data);

    });
}