      
          function getAllMaterials(){
        

          var access_token = localStorage.getItem("access_token");
          var user_status = localStorage.getItem("user_status");


          var xhr1 = new XMLHttpRequest();
          var url1 = "/materials";
          xhr1.open("GET", url1, true);
          xhr1.setRequestHeader("Content-type", "application/json");
          xhr1.setRequestHeader("Accept","application/json");
          xhr1.setRequestHeader("Accept-Language","EN");
          xhr1.setRequestHeader("Authorization","Bearer "+access_token);

          xhr1.onreadystatechange = function(){

           if(xhr1.readyState === xhr1.DONE){
            if(xhr1.status === 200){

              var res = xhr1.response;
              res = JSON.parse(res);
              for (var i = 0; i < res.length; i++) {
                
              
              var id_m = res[i].id;
              var material_name = res[i].material_name;
              var type_id = res[i].type_id;
              var producer_id = res[i].producer_id;
              var producer_name = res[i].producer_name;
              var original_width = res[i].original_width;
              var original_height = res[i].original_height;
              var original_depth = res[i].original_depth;
              var code = res[i].code;
              var description_m = res[i].description_m;
              var image_url = res[i].image_url;
              var materialtype_name = res[i].materialtype_name;
              var created = res[i].created;

              //string for class
      
              var btn_class = "pd-setting-ed";
              var fa = 'fa';
              var pencil = 'fa-pencil-square-o';
              var trash = 'fa-trash';
              var del_material = 'del-material';
              var edit_material = 'edit-material';
              
              //Adding to Dom
              
              var parent = document.getElementById('table_materials');
              var tr = document.createElement("tr");
              var td = document.createElement("td");
              var td1 = document.createElement("td");
              var td2 = document.createElement("td");
              var td3 = document.createElement("td");
              var td4 = document.createElement("td");
              var td5 = document.createElement("td");
              var td6 = document.createElement("td");
              var td7 = document.createElement("td");
              var td8 = document.createElement("td");
              var td10 = document.createElement("td");
              var td11 = document.createElement("td");
              var td12 = document.createElement("td");
              var td13 = document.createElement("td");



              var button1 = document.createElement("button");
              var button2 = document.createElement("button");
              var icon1 = document.createElement("i");
              var icon2 = document.createElement("i");
              var image_m = document.createElement("img");

              image_m.setAttribute('src',image_url);
              image_m.setAttribute('width','70px');


              icon1.setAttribute('title','Edit');
              icon2.setAttribute('title','Delete');

              icon1.classList.add(fa,pencil);
              icon2.classList.add(fa,trash);

              tr.setAttribute('id',id_m+""+id_m);

              button1.setAttribute('id',id_m);
              button2.setAttribute('id',id_m);

              button1.setAttribute('onclick','editmaterial(this)');
              button1.setAttribute('data-toggle','modal');
              button1.setAttribute('data-target','#myModalMaterials');
              button2.setAttribute('onclick','deletematerial(this)');
              
             
              button1.classList.add(btn_class);
              button2.classList.add(btn_class);      
              button1.classList.add(edit_material);
              button2.classList.add(del_material);

              var text_id = document.createTextNode(id_m);
              var text_mat_name = document.createTextNode(material_name);
              var text_prod_name = document.createTextNode(producer_name);
              var text_matype_name = document.createTextNode(materialtype_name);
              var text_width = document.createTextNode(original_width);
              var text_height = document.createTextNode(original_height);
              var text_depth = document.createTextNode(original_depth);
              var text_code = document.createTextNode(code);
              var text_description = document.createTextNode(description_m);
              var text_created = document.createTextNode(created);

              var text_created = document.createTextNode(created);
              
  
              button1.appendChild(icon1);
              button2.appendChild(icon2);

              td11.appendChild(text_id);
              td.appendChild(image_m);
              td1.appendChild(text_mat_name);
              td2.appendChild(text_prod_name);
              td3.appendChild(text_matype_name);
              td4.appendChild(text_width);
              td6.appendChild(text_height);
              td7.appendChild(text_depth);
              td8.appendChild(text_code);
              td13.appendChild(text_description);
              td12.appendChild(text_created);

              td10.appendChild(button1);
              td10.appendChild(button2);



              parent.appendChild(tr);
              tr.appendChild(td11);
              tr.appendChild(td);
              tr.appendChild(td1);
              tr.appendChild(td2);
              tr.appendChild(td3);
              tr.appendChild(td4);
              tr.appendChild(td6);
              tr.appendChild(td7);
              tr.appendChild(td8);
              tr.appendChild(td13);
              tr.appendChild(td12);
              tr.appendChild(td10);
              $(document).ready(function () {
              $('#material_table').DataTable();
              $('.dataTables_length').addClass('bs-select');
            });
              
                
              }
            }
               
            else{ 

                    var res = xhr1.response;
                    response = JSON.parse(res);
                    var message = response.error.message;                    
                     }
            }

        }
              xhr1.send();
      
  }