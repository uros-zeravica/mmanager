		window.onload = function(){


      //  checkign if user is loggin

      var access_token = localStorage.getItem("access_token");

        if (typeof access_token === 'undefined' || access_token === null) {

           window.location.replace("/pages/login.php");

        } 

        // Function to get Profile

        getProfile();


      // Event for Logout

  		var logout = document.getElementById('logout');
      logout.addEventListener('click',function(){

		    var access_token = localStorage.getItem("access_token");

		    var xhr = new XMLHttpRequest();
        var url1 = "/logout";
        xhr.open("POST", url1, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader("Accept","application/json");
        xhr.setRequestHeader("Accept-Language","EN");
        xhr.setRequestHeader("Authorization","Bearer "+access_token);

        xhr.onreadystatechange = function(){
         if(xhr.readyState === xhr.DONE){
          if(xhr.status === 200){
            localStorage.removeItem("access_token");
            localStorage.removeItem("user_status");
  			    location.replace("/pages/login.php");
    		          }
    		  else{ 

                  var res = xhr.response;
                  response = JSON.parse(res);
                  var message = response.error.message;
                  alert(message);
                  location.replace("/pages/login.php");

                   }
             }
          }
            xhr.send();

    });

           // http req for loadProfie

          function getProfile(){

          var access_token = localStorage.getItem("access_token");
          var user_status = localStorage.getItem("user_status");

          var xhr = new XMLHttpRequest();
          var url1 = "/profile";
          xhr.open("GET", url1, true);
          xhr.setRequestHeader("Content-type", "application/json");
          xhr.setRequestHeader("Accept","application/json");
          xhr.setRequestHeader("Accept-Language","EN");
          xhr.setRequestHeader("Authorization","Bearer "+access_token);

          xhr.onreadystatechange = function(){
           if(xhr.readyState === xhr.DONE){
            if(xhr.status === 200){

               var res = xhr.response;
              response = JSON.parse(res);
              var first_name = response.first_name;
              var last_name = response.last_name;
              var email = response.username;
              
              
              document.getElementById('dash_profile').innerHTML = first_name + " " + last_name;
              if(user_status == 0){
              document.getElementById('contact').innerHTML = 'User';
              document.getElementById('contact').style.color = '#fff';
              }
              if(user_status == 1){
              document.getElementById('contact').innerHTML = 'Admin';
              document.getElementById('contact').style.color = '#fff';
              }
            }
            else{ 

                    var res = xhr.response;
                    response = JSON.parse(res);
                    var message = response.error.message;
                    document.getElementById("error").style.display = "block";
                    document.getElementById('error').innerHTML = message;
                    
                     }
               }
            }
              xhr.send();

        };

  }