<?php

require __DIR__ . '/../lib/vendor/autoload.php';

require __DIR__ . '/../core/Config/Config.php';
require __DIR__ . '/../core/Config/DatabaseConfig.php';

require __DIR__ . '/../core/Service/BaseService.php';
require __DIR__ . '/../core/Service/AuthService.php';
require __DIR__ . '/../core/Service/ProfileService.php';
require __DIR__ . '/../core/Service/UserService.php';
require __DIR__ . '/../core/Service/AdminService.php';
require __DIR__ . '/../core/Service/OrderService.php';
require __DIR__ . '/../core/Server/CustomHTTPAuthServer.php';

$server = new \Jacwright\RestServer\RestServer('debug');
$server->setAuthHandler(new Mmanager\Server\CustomHTTPAuthServer);
$server->jsonAssoc = true;
$server->addClass('Mmanager\Service\AuthService');
$server->addClass('Mmanager\Service\ProfileService');
$server->addClass('Mmanager\Service\UserService');
$server->addClass('Mmanager\Service\OrderService');	
$server->addClass('Mmanager\Service\AdminService');

$server->handle();

