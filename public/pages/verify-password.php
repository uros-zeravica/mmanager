<?php include './partials/head.php'; ?>
<body>
    

<div class="container">

    <form class="well form-horizontal" method="post" id="contact-form">
    
    <input type="hidden" id="token" name="token" value="<?php print htmlspecialchars($_GET['token'])?>">
    <input type="hidden" id="pin" name="pin" value="<?php print htmlspecialchars($_GET['pin'])?>">
    <fieldset>
    
                <!-- Form Name -->
        <legend><center><h2><b>Reset Password Form</b></h2></center></legend><br>
        
        <!-- Text input-->
               <div class="form-group">
          <label class="col-md-4 control-label">New Password</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
          <input name="password" placeholder="Your new password" class="form-control"  type="password" id="password">
            </div>
          </div>
        </div>
        <!-- Text input-->
               <div class="form-group">
          <label class="col-md-4 control-label">Repeat password</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
          <input name="repeat_password" placeholder="Repeat password" class="form-control"  type="password" id="repeat_password">
            </div>
          </div>
        </div>
      
    <div class="alert alert-danger" id="error"></div>


    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label"></label>
      <div class="col-md-4"><br>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" id="submit" value="Reset">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button><br><br>
      </div>
    </div>

    </fieldset>
    </form>
        </div><!-- /.container -->
    <script src="../js/http-activepass.js"></script>
    <script src="../js/validation-from.js"></script>
    
    
    </body>
    </html>  