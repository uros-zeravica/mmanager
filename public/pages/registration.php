<?php include './partials/head.php'; ?>
<body>
    

<div class="container">

    <form class="well form-horizontal" method="post"  id="contact-form">

    <input type="hidden" id="token" name="token" value="<?php print htmlspecialchars($_GET['token'])?>">
      
                

    <fieldset>

<!-- Form Name -->
<legend><center><h2><b>Register Form</b></h2></center></legend><br>
<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">First Name</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="first_name" placeholder="firstname" class="form-control"  type="text" id="first_name">
    </div>
  </div>
</div>
<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">Last Name</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="last_name" placeholder="lastname" class="form-control"  type="text" id="last_name">
    </div>
  </div>
</div>
<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="email" placeholder="email" class="form-control"  type="text" id="email">
    </div>
  </div>
</div>


  
<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" >Password</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
  <input name="password" placeholder="password" class="form-control" id="password" type="password">
    </div>
  </div>
</div>

<!--Error message -->
<span class="alert alert-danger" id="error"></span>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4"><br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" id="submit">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
  </div>
  

</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->

<script src="../js/validation-from.js"></script>
<script src="../js/http-request-reg.js"></script>



</body>
</html>  