<?php include './partials/head.php'; ?>
<body>
    

<div class="container">

    <form class="well form-horizontal" action="/profile/password/activation" method="post">
    
    <input type="hidden" id="token" name="token" value="<?php print htmlspecialchars($_GET['token'])?>">
    <fieldset>
    
                <!-- Form Name -->
        <legend><center><h2><b>Change Password Form</b></h2></center></legend><br>
        <!-- Text input-->
               <div class="form-group">
          <label class="col-md-4 control-label">Email</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
          <input name="email" placeholder="Your email" class="form-control"  type="text" id="email">
            </div>
          </div>
        </div>
        <!-- Text input-->
               <div class="form-group">
          <label class="col-md-4 control-label">Password</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
          <input name="password" placeholder="Your new password" class="form-control"  type="password" id="password">
            </div>
          </div>
        </div>
        <!-- Text input-->
               <div class="form-group">
          <label class="col-md-4 control-label">Repeat password</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
          <input name="repeat_password" placeholder="Repeat password" class="form-control"  type="password" id="repeat_password">
            </div>
          </div>
        </div>
          <!-- Success message -->
    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label"></label>
      <div class="col-md-4"><br>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" id="submit" value="Reset">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button><br><br>
      </div>
    </div>

    </fieldset>
    </form>
    </div>
        </div><!-- /.container -->


    </body>
    </html>  