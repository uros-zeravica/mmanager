<?php include './partials/head.php'; ?>
<body>
    

<div class="container">

    <form class="well form-horizontal"  method="post" id="contact-form">
    
    <fieldset>
    
    <input type="hidden" id="token" name="token" value="<?php print htmlspecialchars($_GET['token'])?>">

<!-- Form Name -->
<legend><center><h2><b>Reset Password Form</b></h2></center></legend><br>
<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text" id="email">
    </div>
  </div>
</div>


<!-- Select Basic -->


<span class="alert alert-danger" id="error"></span> 

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4"><br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" id="submit">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button><br><br>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->

<script src="../js/http-request-respsw.js"></script>
<script src="../js/validation-from.js"></script>


</body>
</html>  