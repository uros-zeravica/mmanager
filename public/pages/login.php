  <?php include './partials/head.php'; ?>
<body>
    

<div class="container">

    <form class="well form-horizontal" method="post"  id="contact-form">
    
    <fieldset>
    
    <input type="hidden" id="token" name="token" value="<?php print htmlspecialchars($_GET['token'])?>">

<!-- Form Name -->
<legend><center><h2><b>Login Form</b></h2></center></legend><br>
<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text" id="email">
    </div>
  </div>
</div>
  
<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" >Password</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
  <input name="password" placeholder="Password" class="form-control" id="password" type="password">
    </div>
  </div>
</div>

<!-- Error messager -->
<sapn id="error" class="alert alert-danger"></sapn>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4"><br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" id="submit">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button><br><br>
    <span>Forgot password?&nbsp&nbsp<a href="./reset-password.php">Reset password</a></span><br>
    <span>Don't have account?&nbsp&nbsp<a href="./registration.php">Sign up</a></span>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->

<script src="../js/http-request-log.js"></script>
<script src="../js/validation-from.js"></script>


</body>
</html>  