# mmanager


DEVELOPER INFO:


1. Git repository

https://bitbucket.org/uros-zeravica/mmanager/src/master/

    - Vendor-Libs are also in the git
2. Content collate and define a HOST (MAMP or Docker)
    - DOCUMENT_ROOT should point to the public folder in the project
3. In the core / Config / Config.php, set the following constants
    - ROOT_URL - the host of the host address of the project
    - ROOT_FOLDER - path to the project on the disk
4. In the core / Config / DatabaseConfig.php enter the settings for the database

5.Run and Seed Migrations

- in ROOT_FOLDER / lib there is a phinx.yml conf file for migrations

(by default, the development mode is set)

Open terminal and lib folder

./vendor/bin/phinx migrate -e development command triggers migration

php vendor / bin / phinx seed: run command serves for seeding the database.

For each new seeding, a separate command should be performed

eg php vendor / bin / phinx seed: run -s NameClassSeeder

After successful migration and seeding you can log in as

Admin

username: uros.zeravica@kelcix.com password: root

User

 username: zeravicauros@gmail.com password: user







INSTALLATION GUIDE :



1.Prerequisites

a.For this project you will need PHP 7.0 version 

installation guide link for WINDOWS

https://www.sitepoint.com/how- ... tall-php-on-windows/

installation guide for UNIX

https://www.rosehosting.com/bl ... 7-2-on-ubuntu-16-04/

b.Next you need MySQL 5.7 version  and APACHE

installation guide for MySQL 5.7https://dev.mysql.com/doc/mysql-getting-started/en/

installation guide for APACHE http://httpd.apache.org/docs/2.4/install.html

c.Next you will need to have composer install global on your machine

https://getcomposer.org/doc/00-intro.md

When you install composer check for dependencies 

Go to ROOT_FOLDER/lib and command 

composer install   to check if there is somthing to install or update

d.Next install dependencies for composer 

d1) Phinx package for migrations installation link

http://docs.phinx.org/en/latest/install.html

d2) JackWright package for REST/API installation link

https://github.com/messagebird/php-rest-api

d3) PHPMailer package for sending mail-s installation link

https://github.com/PHPMailer/PHPMailer

 

2.Git repository link

git clone http://188.213.171.193:7990/scm/mmanager/mmanager.git

 Set virtual host


sudo chown -R cyber ~/gitpublisher/mmanager
sudo chgrp -R www-data ~/gitpublisher/mmanager
sudo chmod -R 775 ~/gitpublisher/mmanager

sudo cp -a mmanager/ /var/www/


hosts file
127.0.0.1      mmanager

<VirtualHost *:80>
    # The ServerName directive sets the request scheme, hostname and port that
    # the server uses to identify itself. This is used when creating
    # redirection URLs. In the context of virtual hosts, the ServerName
    # specifies what hostname must appear in the request's Host: header to
    # match this virtual host. For the default virtual host (this file) this
    # value is not decisive as it is used as a last resort host regardless.
    # However, you must set it for any further virtual host explicitly.
    #ServerName www.example.com

    ServerAdmin webmaster@localhost
    ServerName mmanager
   
    DocumentRoot /var/www/mmanager/public
        <Directory /var/www/mmanager/public>
            DirectoryIndex index.php   
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
         </Directory>

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the loglevel for particular
    # modules, e.g.
    #LogLevel info ssl:warn
   
   
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
   

    # For most configuration files from conf-available/, which are
    # enabled or disabled at a global level, it is possible to
    # include a line for only one particular virtual host. For example the
    # following line enables the CGI configuration for this host only
    # after it has been globally disabled with "a2disconf".
    #Include conf-available/serve-cgi-bin.conf
</VirtualHost>
 

3.Set configuration for project and DB configuration

a1 Go to ROOT_FOLDER/core/config/Config.php

<?php
namespace Mmanager\Config;

class Config {

  const ROOT_URL = 'http://mmanager';
  const ROOT_FOLDER = '/var/www/mmanager';

  const ACCESS_TOKEN_LIFETIME = 2592000;

  const FB_APP_ID = '150560771787719';
  const FB_APP_SECRET = 'e9badd2552067f64eea3d2d4949e53fa';
  const FB_VERSION = 'v2.9';
  

  const WELCOME_PAGE = self::ROOT_URL . '/welcome.php';

}

?>

a2 Go to ROOT_FOLDER/core/config/DatabaseConfig.php 

<?php

namespace Mmanager\Config;

class DatabaseConfig {

  const HOST = 'localhost';
  const PORT = '3306';
  const DATABASE = 'mmanager';
  const USER = 'root';
  const PASSWORD = 'root';

  const DSN = 'mysql:host=' . DatabaseConfig::HOST . ':' . DatabaseConfig::PORT . ';dbname=' . DatabaseConfig::DATABASE;


}

?>

3.Set migrations parametars for phinx 

Go to ROOT_FOLDER/lib/phinx.yml file and your file should look like this

paths:
    migrations: '%%PHINX_CONFIG_DIR%%/db/migrations'
    seeds: '%%PHINX_CONFIG_DIR%%/db/seeds'

environments:
    default_migration_table: phinxlog
    default_database: development
    production:
        adapter: mysql
        host: localhost
        name: production_db
        user: root
        pass: ''
        port: 3306
        charset: utf8

    development:
        adapter: mysql
        host: localhost
        name: mmanager
        user: root
        pass: 'root'
        port: 3306
        charset: utf8


    testing:
        adapter: mysql
        host: localhost
        name: testing_db
        user: root
        pass: ''
        port: 3306
        charset: utf8

version_order: creation

You can then user command in terminal in ROOT_FOLDER/lib 

./vendor/bin/phinx migrate -e development for run migrations

php vendor/bin/phinx seed:run to run seeds 

