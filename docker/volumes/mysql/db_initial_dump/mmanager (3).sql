-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 25, 2019 at 05:17 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `address` varchar(80) NOT NULL,
  `address1` varchar(80) DEFAULT NULL,
  `city` varchar(60) NOT NULL,
  `state` varchar(60) NOT NULL,
  `country` int(11) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `email` varchar(160) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_delivery_start` timestamp NULL DEFAULT NULL,
  `date_delivery` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_types`
--

CREATE TABLE `delivery_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `type_id` int(11) NOT NULL,
  `original_width` float NOT NULL,
  `original_height` float NOT NULL,
  `original_depth` float NOT NULL,
  `code` varchar(255) NOT NULL,
  `description_m` text,
  `image_url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `materials_custom`
--

CREATE TABLE `materials_custom` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `type_id` int(11) NOT NULL,
  `original_width` float NOT NULL,
  `original_height` float NOT NULL,
  `original_depth` float NOT NULL,
  `code` varchar(225) NOT NULL,
  `description_m` text,
  `image_url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `material_general_type`
--

CREATE TABLE `material_general_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `material_type`
--

CREATE TABLE `material_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_in_progress` timestamp NULL DEFAULT NULL,
  `date_finished` timestamp NULL DEFAULT NULL,
  `date_delivery` timestamp NULL DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `payment` tinyint(1) NOT NULL DEFAULT '0',
  `price` float DEFAULT '0',
  `adress` varchar(80) NOT NULL,
  `adress1` varchar(80) DEFAULT NULL,
  `state` varchar(80) NOT NULL,
  `country` int(11) NOT NULL,
  `delivery_type_id` int(11) NOT NULL,
  `supplied_by_users_id` int(11) DEFAULT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `requested_date` timestamp NULL DEFAULT NULL,
  `photo_attach_image` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(225) NOT NULL,
  `material_id` int(225) NOT NULL,
  `quantity` int(11) NOT NULL,
  `materials_custom_id` int(11) NOT NULL,
  `photo_attach_image` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items_elements`
--

CREATE TABLE `order_items_elements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_items_id` int(225) NOT NULL,
  `width` float NOT NULL,
  `height` float NOT NULL,
  `direction` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_item_calculation`
--

CREATE TABLE `order_item_calculation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_items_id` int(225) NOT NULL,
  `width_cut` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20180710120021, 'CreateOauthMigration', '2018-12-19 15:47:16', '2018-12-19 15:47:16', 0),
(20180802073125, 'CreateTempPassword', '2018-12-19 15:47:16', '2018-12-19 15:47:16', 0),
(20180816093738, 'CreateTempUser', '2018-12-19 15:47:16', '2018-12-19 15:47:16', 0),
(20180822121212, 'CreateProducers', '2018-12-19 15:47:16', '2018-12-19 15:47:16', 0),
(20180822123123, 'CreateCountries', '2018-12-19 15:47:16', '2018-12-19 15:47:16', 0),
(20180822123421, 'CreateMaterials', '2018-12-19 15:47:16', '2018-12-19 15:47:17', 0),
(20180822124425, 'CreateMaterialType', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822125926, 'CreateOrder', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822131718, 'CreateDeliveryTypes', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822131727, 'CreateStatusTypes', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822132317, 'CreateOrderItems', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822133522, 'CreateOrderItemsElements', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822134258, 'CreateOrderItemCalculation', '2018-12-19 15:47:17', '2018-12-19 15:47:17', 0),
(20180822141226, 'CreateUserTypes', '2018-12-19 15:47:17', '2018-12-19 15:47:18', 0),
(20180824075838, 'UpdateOauthClients', '2018-12-19 15:47:18', '2018-12-19 15:47:18', 0),
(20181001135728, 'CreateWerehouseItems', '2018-12-19 15:47:18', '2018-12-19 15:47:18', 0),
(20181001135745, 'CreateWerehouse', '2018-12-19 15:47:18', '2018-12-19 15:47:18', 0),
(20181001141746, 'CreateDelivery', '2018-12-19 15:47:18', '2018-12-19 15:47:18', 0),
(20181001144851, 'CreateMaterialGeneralType', '2018-12-19 15:47:18', '2018-12-19 15:47:18', 0),
(20181001145324, 'CreateMaterialsCustom', '2018-12-19 15:47:18', '2018-12-19 15:47:18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `producers`
--

CREATE TABLE `producers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` varchar(80) NOT NULL,
  `address1` varchar(80) DEFAULT NULL,
  `city` varchar(60) NOT NULL,
  `state` varchar(60) NOT NULL,
  `country` int(11) NOT NULL,
  `email` varchar(225) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `web` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `status_types`
--

CREATE TABLE `status_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temp_password`
--

CREATE TABLE `temp_password` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `password` varchar(225) NOT NULL,
  `token` varchar(225) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temp_user`
--

CREATE TABLE `temp_user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `activation_code` varchar(1024) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` varchar(256) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `first_name` varchar(256) DEFAULT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `status`, `first_name`, `last_name`, `modified`, `created`) VALUES
(16, 'uros.zeravica@kelcix.com', '$2y$10$EUNa8IeG4YzEhWCsV/Gen.fst4nDvIxZe5qozsoUlSqYc5LPDqzXi', 0, 'Zeravica', 'Uros', '2019-01-14 11:55:39', '2019-01-03 13:27:05'),
(20, 'zearavicauros@gmail.com', '$2y$10$kEk3GaRVl0ztl1dI4dyImutQD3PsKNyYQGZ75pZ13lmEXq.WyXTXG', 0, 'Sava', 'Savanovic', '2019-01-22 09:19:43', '2019-01-10 14:50:23'),
(27, 'zeravicauros@gmail.com', '$2y$10$OfkFR7ke9E7QhX2Mk7I16OnLUNSRy4T4Y2fCkCCUADXIfX6VSqCX.', 1, 'Sava', 'Peric', '2019-01-15 13:45:07', '2019-01-14 14:45:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_token`
--

CREATE TABLE `user_access_token` (
  `access_token` varchar(256) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_access_token`
--

INSERT INTO `user_access_token` (`access_token`, `user_id`, `expires`) VALUES
('035fd958c18a455c2b375266516aa7ea9b1a4307', 27, '2019-02-17 08:56:21'),
('1d42231f767760ca264c871719556414665502d3', 27, '2019-02-20 08:02:30'),
('2329ce127ec1ae60fa650393e1ac818f5d3c8100', 27, '2019-02-14 15:10:21'),
('331811ef96a069f57137323c5156908f9914e7f5', 27, '2019-02-21 16:21:41'),
('49b9d0fa299fab9bf2ed86db9f61428bd764ba8a', 20, '2019-02-21 09:41:54'),
('4f9fab699dc06709440e27b6b69d31e76236e1d7', 20, '2019-02-20 10:49:41'),
('5a8d221882e1fe0de58e69cd21624950222d1051', 27, '2019-02-20 14:02:51'),
('65af9c7d8f53d6519510d336aa9bedc84d974a18', 27, '2019-02-20 10:48:59'),
('68161be656dfe57adf68dc226b8d5c8cb7dbb15b', 27, '2019-02-21 15:13:30'),
('6b6599c9a6d385a08187b0bcbc3cdbd8d10bd821', 27, '2019-02-24 16:07:30'),
('7110f8c40ce4697f99ee7a407f1a8b2efe787e29', 27, '2019-02-20 16:05:04'),
('837a90e5a94c5668c3184832db08c746daf48670', 27, '2019-02-15 08:09:19'),
('9279c1c7e7d6dd014a353ff3673ca87686335f15', 27, '2019-02-17 16:04:42'),
('b36febf8358fce5387d2e207eb614b5a8c7453bc', 20, '2019-02-21 09:20:50'),
('b831b3214129751d580613b9226704066249d34a', 20, '2019-02-21 09:20:36'),
('c530b4dca12d2e6b0bb9ed7ca7301414e4ff446f', 27, '2019-02-22 08:49:57'),
('db4d15c1f035b95bec53125bdc4844431a59ef6d', 20, '2019-02-21 09:19:51'),
('e2078ec3e95ba11256ab67670214409f500fcfb4', 27, '2019-02-20 09:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `werehouse`
--

CREATE TABLE `werehouse` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `vat` float NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `werehouse_item`
--

CREATE TABLE `werehouse_item` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `werehouse_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `LOT_number` varchar(200) NOT NULL,
  `date_finished` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_types`
--
ALTER TABLE `delivery_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materials_custom`
--
ALTER TABLE `materials_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_general_type`
--
ALTER TABLE `material_general_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `material_type`
--
ALTER TABLE `material_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items_elements`
--
ALTER TABLE `order_items_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item_calculation`
--
ALTER TABLE `order_item_calculation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `producers`
--
ALTER TABLE `producers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_types`
--
ALTER TABLE `status_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `temp_password`
--
ALTER TABLE `temp_password`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `temp_user`
--
ALTER TABLE `temp_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `username_2` (`username`),
  ADD KEY `activation_code` (`activation_code`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_id_token` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- Indexes for table `user_access_token`
--
ALTER TABLE `user_access_token`
  ADD PRIMARY KEY (`access_token`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `werehouse`
--
ALTER TABLE `werehouse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `werehouse_item`
--
ALTER TABLE `werehouse_item`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `delivery_types`
--
ALTER TABLE `delivery_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `materials_custom`
--
ALTER TABLE `materials_custom`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material_general_type`
--
ALTER TABLE `material_general_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material_type`
--
ALTER TABLE `material_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_items_elements`
--
ALTER TABLE `order_items_elements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_item_calculation`
--
ALTER TABLE `order_item_calculation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `producers`
--
ALTER TABLE `producers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status_types`
--
ALTER TABLE `status_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_password`
--
ALTER TABLE `temp_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_user`
--
ALTER TABLE `temp_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `werehouse`
--
ALTER TABLE `werehouse`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `werehouse_item`
--
ALTER TABLE `werehouse_item`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `temp_password`
--
ALTER TABLE `temp_password`
  ADD CONSTRAINT `fk_user_id_temp_password` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_id_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_access_token`
--
ALTER TABLE `user_access_token`
  ADD CONSTRAINT `fk_user_id_access_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
