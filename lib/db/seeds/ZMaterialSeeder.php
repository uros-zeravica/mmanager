<?php


use Phinx\Seed\AbstractSeed;

class ZMaterialSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $img_url = ['https://www.optis-world.com/var/website/storage/images/media/images/products/oms2-%7C-scan-an-infinite-variety-of-materials-and-colors/5991-1-eng-US/OMS2-%7C-Scan-an-infinite-variety-of-materials-and-colors_reference.jpg','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKULlFDGjsoXoH1oNEs1r9DUH299C6fSWgNCTqgpkEx5zwg2lz','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3GL5QE1BEfldlbI8Ls3VXeJs14ar7nkuzFJSR5yQM2G_5URIrpw'];
        for ($i = 0; $i < 100; $i++) {
        $type_id = rand(1,2);
        $producer_id = rand(1,10);
        $original_width = rand(1,1000);
        $original_height = rand(1,1000);
        $original_depth = rand(1,1000);
        $code = rand(1000,10000);
        $rand_img = rand(0,2);
            $data[] = [
                'material_name'       => $faker->name,
                'type_id'             =>   $type_id,
                'producer_id'         => $producer_id,
                'original_width'      => $original_width,
                'original_height'     => $original_height,
                'original_depth'      => $original_depth,
                'code'                => $code,
                'description_m'       => $faker->text,
                'image_url'           => $img_url[$rand_img],
                'created'             => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('materials')->insert($data)->save();
    
    }
}
