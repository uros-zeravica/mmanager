<?php


use Phinx\Seed\AbstractSeed;

class StatusTypesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id'    => '1',
                'name'    => 'Created',
            ],[
                'id'    => '2',
                'name'    => 'Assigned',
            ],[
                'id'    => '3',
                'name'    => 'Finished',
            ],[
                'id'    => '4',
                'name'    => 'InWarehouse',
            ],[
                'id'    => '5',
                'name'    => 'Delivered',
            ]
        ];

        $materialtype = $this->table('status_types');
        $materialtype->insert($data)
              ->save();
    }
}
