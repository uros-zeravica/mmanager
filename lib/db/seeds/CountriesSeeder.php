<?php


use Phinx\Seed\AbstractSeed;

class CountriesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
               
         $countries = file_get_contents('../public/dashboard/data/countries.json');
        $countries = json_decode($countries,true);
        //die(var_dump($countries));
        foreach ($countries as $country) {
            for ($i=0; $i < count($country); $i++) { 
                
            $data[] = [
                'name' => $country[$i],
            ];
        }
    }


            $countries = $this->table('countries');
            $countries->insert($data)    
              ->save();
}
}
