<?php


use Phinx\Seed\AbstractSeed;

class ProducerSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 50; $i++) {
        $country = rand(1,20);
            $data[] = [
                'producer_name'       => $faker->name,
                'address'             =>   $faker->address,
                'address1'            => $faker->address,
                'city'                => $faker->cityPrefix,
                'state'               => $faker->state,
                'country'             => $country,
                'email'               => $faker->email,
                'phone'               => $faker->phoneNumber,
                'web'                 => $faker->freeEmailDomain,
                'created'             => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('producers')->insert($data)->save();
    }
}
