<?php


use Phinx\Seed\AbstractSeed;

class DeliveryTypesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
   public function run()
    {
        $data = [
            [
                'id'    => '1',
                'name'    => 'NoDelivery',
            ],[
                'id'    => '2',
                'name'    => 'OnLocation',
            ],[
                'id'    => '3',
                'name'    => 'Courrier',
            ],[
                'id'    => '4',
                'name'    => 'PostOffice',
            ]
        ];

        $materialtype = $this->table('delivery_types');
        $materialtype->insert($data)
              ->save();
    }
}
