<?php


use Phinx\Seed\AbstractSeed;

class MaterialTypeSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id'    => '1',
                'materialtype_name'    => 'material type metal',
                'general_type_id'    => '1',
            ],[
                'id'    => '2',
                'materialtype_name'    => 'material type plastic',
                'general_type_id'    => '2',
            ]
        ];

        $materialtype = $this->table('material_type');
        $materialtype->insert($data)
              ->save();
    }
}
