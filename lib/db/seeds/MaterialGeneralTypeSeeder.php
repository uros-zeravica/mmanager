<?php


use Phinx\Seed\AbstractSeed;

class MaterialGeneralTypeSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id'    => '1',
                'generaltype_name'    => 'metal',
            ],[
                'id'    => '2',
                'generaltype_name'    => 'plastic',
            ]
        ];

        $materialgeneraltype = $this->table('material_general_type');
        $materialgeneraltype->insert($data)
              ->save();
    }
}
