<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'username'    => 'uros.zeravica@kelcix.com',
                'status'    => '1',
                'password'    => '$2y$10$y/Q8wnlxOV5Ec6CjM286aeW0OwvJvwdN9OgspyVD.wDkPahbw02N2',
                'first_name'    => 'Root',
                'last_name'    => 'Root',
                'created' => date('Y-m-d H:i:s'),
            ],[
                'username'    => 'zeravicauros@gmail.com',
                'status'    => '0',
                'password'    => '$2y$10$8yROnKBqWnuh/rRPwGo5EupEZKmPXuGj2BRAxpl4jwddv9ibKNjzC',
                'first_name'    => 'User',
                'last_name'    => 'User',
                'created' => date('Y-m-d H:i:s'),
            ]
        ];

        $user = $this->table('user');
        $user->insert($data)
              ->save();
    }
}
