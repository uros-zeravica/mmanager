<?php


use Phinx\Migration\AbstractMigration;

class OrderItemsElementsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $orderItemsElements = $this->table('order_items_elements', ['primary_key' => 'id']);
        $orderItemsElements 
                ->addColumn('order_items_id', 'biginteger', ['limit' => 225,"length" => "20",'signed' => false,'null' => false])
                ->addColumn('width', 'float')
                ->addColumn('height', 'float')
                ->addColumn('direction', 'string', ['limit' => 80])
                ->addForeignKey('order_items_id', 'order_items', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->create();
        
    }
    
    
    public function down()
    {
        $this->dropTable('order_items_elements');
    }
}
