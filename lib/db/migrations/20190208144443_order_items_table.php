<?php


use Phinx\Migration\AbstractMigration;

class OrderItemsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $orderItems = $this->table('order_items', ['id' => false, 'primary_key' => ['id']]);
        $orderItems->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]);
        $orderItems 
                ->addColumn('order_id', 'biginteger', ['limit' => 225,"length" => "20",'signed' => false,'null' => false])
                ->addColumn('material_id', 'biginteger', ['limit' => 225,"length" => "20",'signed' => false,'null' => false])
                ->addColumn('quantity', 'integer')
                ->addColumn('materials_custom_id', 'integer')
                ->addColumn('photo_attach_image', 'text', ['null' => true])
                ->addForeignKey('order_id', 'order', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->addForeignKey('material_id', 'materials', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->create();
        
        
    }
    
    
    public function down()
    {
        $this->dropTable('order_items');
    }
}
