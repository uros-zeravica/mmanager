<?php


use Phinx\Migration\AbstractMigration;

class DeliveryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {

        $delivery = $this->table('delivery', ['id' => false, 'primary_key' => ['id']]);
        $delivery->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]);

        $delivery
                ->addColumn('first_name', 'string', ['limit' => 255, 'null' => true])
                ->addColumn('last_name', 'string', ['limit' => 255, 'null' => true])
                ->addColumn('address', 'string', ['limit' => 80])
                ->addColumn('address1', 'string', ['limit' => 80, 'null' => true])
                ->addColumn('city', 'string', ['limit' => 60])
                ->addColumn('state', 'string', ['limit' => 60])
                ->addColumn('country', 'biginteger',["length" => "20",'signed' => false,'null' => false])
                ->addColumn('phone', 'string', ['limit' => 60])
                ->addColumn('email', 'string', ['limit' => 160])
                ->addColumn('date_created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('date_delivery_start', 'timestamp', ['null' => true])
                ->addColumn('date_delivery', 'integer')
                ->addForeignKey('country', 'countries', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->create();
        
        
    }
    
    
    public function down()
    {
        $this->dropTable('delivery');
    }
}
