<?php


use Phinx\Migration\AbstractMigration;

class OrderTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $order = $this->table('order', ['id' => false, 'primary_key' => ['id']]);
        $order->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]);
        $order
                ->addColumn('user_id', 'biginteger',["length" => "20",'signed' => false,'null' => false])
                ->addColumn('date_created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('date_in_progress', 'timestamp', ['null' => true])
                ->addColumn('date_finished', 'timestamp', ['null' => true])
                ->addColumn('date_delivery', 'timestamp', ['null' => true])
                ->addColumn('status_id', 'integer', ['default' => "1"])
                ->addColumn('payment', 'boolean', ['default' => "0"])
                ->addColumn('price', 'float', ['null' => false, 'default' => "0"])
                ->addColumn('adress', 'string', ['limit' => 80])
                ->addColumn('adress1', 'string', ['limit' => 80, 'null' => true])
                ->addColumn('state', 'string', ['limit' => 80])
                ->addColumn('country', 'biginteger',["length" => "20",'signed' => false,'null' => false])
                ->addColumn('delivery_type_id', 'integer')
                ->addColumn('supplied_by_users_id', 'integer', [ 'null' => false,'default' => "0"])
                ->addColumn('delivery_id', 'biginteger', ["length" => "20",'signed' => false,'null' => false])
                ->addColumn('requested_date', 'timestamp', ['null' => true])
                ->addColumn('photo_attach_image', 'text', ['null' => true])
                ->addForeignKey('user_id', 'user', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->addForeignKey('status_id', 'status_types', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->addForeignKey('country', 'countries', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->addForeignKey('delivery_type_id', 'delivery_types', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->addForeignKey('delivery_id', 'delivery', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))
                ->create();
        
    }
    
    
    public function down()
    {
        $this->dropTable('order');
    }
}
