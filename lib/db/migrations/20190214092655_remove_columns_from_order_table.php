<?php


use Phinx\Migration\AbstractMigration;

class RemoveColumnsFromOrderTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
        public function up()
        {
        $table = $this->table('order');
        $table
            ->removeColumn('adress')
            ->removeColumn('adress1')
            ->removeColumn('state')
            ->dropForeignKey('country')
            ->changeColumn('delivery_id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'default' => "1"])
              ->save();
        }
    public function down(){
        
    }
}
