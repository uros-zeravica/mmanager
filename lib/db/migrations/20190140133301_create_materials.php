<?php


use Phinx\Migration\AbstractMigration;

class CreateMaterials extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    
    public function up()
    {
                $table = $this->table('materials', ['id' => false, 'primary_key' => ['id']]);
                $table->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]);
                $table->addColumn('material_name', 'string', ['limit' => 225]);
                $table->addColumn("type_id", "biginteger", ["length" => "20",'signed' => false,'null' => false]);
                $table->addColumn("producer_id", "biginteger", ["length" => "20",'signed' => false,'null' => false]);
                $table->addColumn('original_width', 'float');
                $table->addColumn('original_height', 'float');
                $table->addColumn('original_depth', 'float');
                $table->addColumn('code', 'string', ['limit' => 255,'null' => true]);
                $table->addColumn('description_m', 'text', ['null' => true]);
                $table->addColumn('image_url', 'text', ['null' => true]);
                $table->addColumn('modified', 'timestamp',array('null' => false,'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'));
                $table->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
                $table->addForeignKey('type_id', 'material_type', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'));
                $table->addForeignKey('producer_id', 'producers', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'));
                $table->addIndex(['type_id']);
                $table->addIndex(['producer_id']);
                $table->create();
    }
     public function down()
        {
            $this->dropTable('materials');
        }
}
