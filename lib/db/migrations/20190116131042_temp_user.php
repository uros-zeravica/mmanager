<?php


use Phinx\Migration\AbstractMigration;

class TempUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
     public function up()
    {

        // Table Name
        
         $table = $this->table('temp_user', ['id' => false, 'primary_key' => ['id']]);
        
        // We don't need to add an "id" column, Phinx adds this by default.
        
        $table->addColumn('id', 'biginteger', ['null' => false,"length" => "20",'identity' => true]);
        $table->addColumn("username", "string", ["length" => "256",'null' => false]);
        $table->addColumn("password", "string", ["length" => "256",'null' => false]);
        $table->addColumn("first_name", "string", ["length" => "256",'null' => true]);
        $table->addColumn("last_name", "string", ["length" => "256",'null' => true]);
        $table->addColumn("activation_code", "string", ["length" => "256",'null' => false]);
        $table->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
        $table->addIndex(['username', 'activation_code'], ['unique' => true]);
        
        $table->create();

    }   
    public function down()
    {
        $this->dropTable('temp_user');
    }
}
