<?php


use Phinx\Migration\AbstractMigration;

class TempPassword extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
            $table = $this->table('temp_password', ['id' => false, 'primary_key' => ['id']]);
            $table->addColumn('id', 'biginteger', ['null' => false,"length" => "20",'identity' => true]);
            $table->addColumn("user_id", "biginteger", ["length" => "20",'null' => false,'signed' => false]);
            $table->addColumn('password', 'string', ['null' => false,"length" => "225"]);
            $table->addColumn('token', 'string', ['null' => false,"length" => "225"]);
            $table->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
            $table->addForeignKey('user_id', 'user', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'));
            $table->addIndex(['user_id']);
            $table->create();
    }
    
    public function down()
    {
        $this->dropTable('temp_password');
    }
}
