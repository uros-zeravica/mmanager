<?php


use Phinx\Migration\AbstractMigration;

class CreateMaterialType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
                $table = $this->table('material_type', ['id' => false, 'primary_key' => ['id']]);

                $table->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]);
                $table->addColumn('materialtype_name', 'string', ['limit' => 225]);
                $table->addColumn('general_type_id', 'biginteger',["length" => "20",'signed' => false,'null' => false]);
                $table->addIndex(['materialtype_name'], ['unique' => true]);
                $table->addForeignKey('general_type_id', 'material_general_type', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'));
                $table->create();
    }
    public function down()
        {
            $this->dropTable('material_type');
        }
}
