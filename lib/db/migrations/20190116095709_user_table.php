<?php


use Phinx\Migration\AbstractMigration;

class UserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('user', ['id' => false, 'primary_key' => ['id']]);
        $table->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]);
        $table->addColumn("username", "string", ["length" => "256",'null' => false]);
        $table->addColumn("password", "string", ["length" => "256",'null' => false]);
        $table->addColumn("status", "integer", ["length" => "6",'null' => true]);
        $table->addColumn("first_name", "string", ["length" => "256",'null' => true]);
        $table->addColumn("last_name", "string", ["length" => "256",'null' => true]);
        $table->addColumn('address', 'string', ['limit' => 80,'null' => true]);
        $table->addColumn('address1', 'string', ['limit' => 80, 'null' => true]);
        $table->addColumn('city', 'string', ['limit' => 60,'null' => true]);
        $table->addColumn('state', 'string', ['limit' => 60,'null' => true]);
        $table->addColumn('country', 'integer',['null' => true]);
        $table->addColumn('phone', 'string', ['limit' => 60,'null' => true]);
        $table->addColumn('modified', 'timestamp',array('null' => false,'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'));
        $table->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
        $table->addIndex(['username'], ['unique' => true]);
        $table->create();
    }
    public function down()
    {
        $this->dropTable('user');
    }
}
