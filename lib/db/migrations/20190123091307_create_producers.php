<?php


use Phinx\Migration\AbstractMigration;

class CreateProducers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {

        $table = $this->table('producers', ['id' => false, 'primary_key' => ['id']]);
        $table
                ->addColumn('id', 'biginteger', ["length" => "20",'signed' => false,'null' => false,'identity' => true]) 
                ->addColumn('producer_name', 'string', ['limit' => 225])
                ->addColumn('address', 'string', ['limit' => 80, 'null' => true])
                ->addColumn('address1', 'string', ['limit' => 80, 'null' => true])
                ->addColumn('city', 'string', ['limit' => 60, 'null' => true])
                ->addColumn('state', 'string', ['limit' => 60, 'null' => true])
                ->addColumn('country', 'biginteger',["length" => "20",'signed' => false,'null' => false])
                ->addColumn('email', 'string', ['limit' => 225])
                ->addColumn('phone', 'string', ['limit' => 60, 'null' => true])
                ->addColumn('web', 'string', ['limit' => 80, 'null' => true])
                ->addColumn('modified', 'timestamp',array('null' => false,'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'))
                ->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP'])
                ->addForeignKey('country', 'countries', 'id', array('delete'=> 'CASCADE', 'update'=> 'RESTRICT'))



                ->create();
        
    }
    
    public function down()
        {
            $this->dropTable('producers');
        }
    
}
